#ifndef __delay_H
#define	__delay_H

#include "stm32f10x.h"

void delay_ms(u16 time);

void  timer2_init( void );
void  timer3_init( void );

void timer2_NVIC_configuration(void);
void timer3_NVIC_configuration(void);

void RTC_NVIC_Configuration(void);
void RTC_int(void);
void RTC_set_time(u32 time);


#endif
