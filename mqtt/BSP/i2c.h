#ifndef __I2C_H
#define __I2C_H

typedef enum I2C_BUS
{
    I2C_BUS1 =0,
    I2C_BUS2
}I2C_BUS_ENUM;



/* export function for callling */
void  i2c_gpio_config( I2C_BUS_ENUM i2c_bus );
void  i2c_mode_config( I2C_BUS_ENUM i2c_bus );
void  i2c_bus1_write_bytes(u8 devaddr, u8 subaddr, u8 *buffer,u8 length);
void i2c_bus1_read_bytes(u8 devaddr,u8 subaddr,u8 *buffer , u8 length);

void  i2c_bus2_write_bytes(u8 devaddr, u8 subaddr, u8 *buffer,u8 length);
void i2c_bus2_read_bytes(u8 devaddr,u8 subaddr,u8 *buffer, u8 length);


#endif /* __I2C_H */
