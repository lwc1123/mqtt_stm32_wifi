#include "includes.h"

/*****************************************************************************
* funcation name   : BSP_Init
*
* input  : 
*
* Output  :
* 
* Return Value : void
* 
* descryption: some basic peripheral initialized
*
*****************************************************************************/
void BSP_Init(void)
{
        SysTick_init();		//initialize and enable systick timer
        LED_GPIO_Config();  
        Key_GPIO_Config();
        timer2_NVIC_configuration();
        timer3_NVIC_configuration();
        RTC_NVIC_Configuration();
//        RTC_int();
        timer2_init();   //timer 
        timer3_init();
        wifi_module_gpio_config();
        //USART1_Config();
        uart1_config_init(9600);
        uart2_config_init(115200);
        uart3_config_init(115200);
}

void SysTick_init(void)
{
    SysTick_Config(SystemCoreClock/OS_TICKS_PER_SEC);//initialize and enable systick timer
}
