/******************************************************************************

  Copyright (C), 2001-2011, CSST  Co., Ltd.

 ******************************************************************************
  File Name     : Key.c
  
  Version       : Initial Draft
  
  Author        : kingerzhou
  
  Created       : 2015/11/5
  
  Last Modified :
  
  Description   : this module is to handle all io and keys
  
******************************************************************************/

#include "stm32f10x.h"
#include "Key.h"
#include "delay.h"
#include "includes.h"

/*****************************************************************************
* funcation name   : Key_GPIO_Config
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
void Key_GPIO_Config(void)
{ 
 	GPIO_InitTypeDef GPIO_InitStructure;

 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_13|GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
 	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 
	GPIO_Init(GPIOA, &GPIO_InitStructure);
 
	 /**********************
	 1.执行端口重映射时,复用功能时钟得使能:RCC_APB2Periph_AFIO
	 
	 2.  &1.GPIO_Remap_SWJ_Disable: !< Full SWJ Disabled (JTAG-DP + SW-DP)
	      此时PA13|PA14|PA15|PB3|PB4都可作为普通IO用了
	 
	 为了保存某些调试端口,GPIO_Remap_SWJ_Disable也可选择为下面两种模式：
	  
	     &2.GPIO_Remap_SWJ_JTAGDisable: !< JTAG-DP Disabled and SW-DP Enabled
	     此时PA15|PB3|PB4可作为普通IO用了
	  
	     &3.GPIO_Remap_SWJ_NoJTRST: !< Full SWJ Enabled (JTAG-DP + SW-DP) but without JTRST
	     此时只有PB4可作为普通IO用了 
	 **********************/		
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOC|RCC_APB2Periph_AFIO, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);  //使能禁止JTAG

	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;  
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
 	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
}

u8 mqtt_pub_key_scan(void)
{
	u8 ret = 0;
	if(0 == KEY2)
	{
		ret = 2;
	}
	else if(0 == KEY_BROKEN)
	{
		ret = 3;
	}

	return ret;
}


/*****************************************************************************
* funcation name   : KEY_Scan
*
* input  : 
*
* Output  :
* 
* Return Value : unsigned
* 
* descryption:
*
*****************************************************************************/
s8 KEY_Scan(void)
{	 
        static char key_up=1;
        static char key_up2=1;
        static char key_up3=1;
        s8 key_code = -1;
        GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
        if((KEY0==0)||(KEY1==0)||(KEY2 ==1))
        {
            OSTimeDlyHMSM(0,0,0,20);
            if(KEY0==0)
            {
                key_up=1;
            }
            else if((KEY1==0))
            {
                key_up2=1;
            }
            else if((KEY2==1))
            {
                key_up3=1;
            }
        }
        if(KEY0==1&&key_up==1)
        {
            key_up=0;
            key_code = 0;
        }
        else if(KEY1==1&&key_up2==1)
        {
            key_up2=0;
            key_code = 1;
        }
        else if(KEY2==0&&key_up3==1)
        {
            key_up3=0;
            key_code = 2;
        }    
        return key_code;
}

/*****************************************************************************
* funcation name   : bluetooth_module_gpio_config
*
* input  : none
*
* Output  :none
* 
* Return Value : void
* 
* descryption: initialize bluetooth module gpio
*
*****************************************************************************/
void  bluetooth_module_gpio_config( void )
{
        GPIO_InitTypeDef GPIO_InitStructure;

        /* gpio as output */
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
        GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_12|GPIO_Pin_8; //PA12,PA8
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
        GPIO_Init(GPIOA, &GPIO_InitStructure);
        GPIO_ResetBits(GPIOA,GPIO_Pin_12);   // BRT low, make module not entry standby
        GPIO_SetBits(GPIOA,GPIO_Pin_8);   //MODE_CTL, high is through mode

         RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);   
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;	    //PC8		 
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
        GPIO_Init(GPIOC, &GPIO_InitStructure);	
        GPIO_SetBits(GPIOC,GPIO_Pin_8);  //BT_CTL, high is broadcast

        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);   
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;	    //PB8		 
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
        GPIO_Init(GPIOB, &GPIO_InitStructure);	
        GPIO_SetBits(GPIOB,GPIO_Pin_8); //RESET, low to reset bluetooth module

        /* gpio as input */
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
        GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_11;//PA11 CONN_STAT ,connect display ,high is connected
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
        GPIO_Init(GPIOA, &GPIO_InitStructure);

         RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
        GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_9|GPIO_Pin_7;//PC9 => BRT, PC7 => BTT
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
        GPIO_Init(GPIOC, &GPIO_InitStructure);

}


/*****************************************************************************
* funcation name   : wifi_module_gpio_config
*
* input  : none
*
* Output  :none
* 
* Return Value : void
* 
* descryption: initialize wifi module gpio
*
*****************************************************************************/
void  wifi_module_gpio_config( void )
{
        GPIO_InitTypeDef GPIO_InitStructure;

        /* gpio as output */
         RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);   
        GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;	    //PC6		 
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
        GPIO_Init(GPIOC, &GPIO_InitStructure);	
        GPIO_SetBits(GPIOC,GPIO_Pin_6);  //wifi_module reset

}

/*****************************************************************************
* funcation name   : bluetooth_module_reset
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
void  bluetooth_module_reset( void )
{
    GPIO_ResetBits(GPIOB,GPIO_Pin_8); 
    OSTimeDly(50); 
    GPIO_SetBits(GPIOB,GPIO_Pin_8);
     bluetooth_module_control_BRT_gpio_output(GPIO_LOW);  // out of sleep
}

/*****************************************************************************
* funcation name   : wifi_module_reset
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
void  wifi_module_reset( void )
{
    GPIO_ResetBits(GPIOC,GPIO_Pin_6); 
    OSTimeDly(150); 
    GPIO_SetBits(GPIOC,GPIO_Pin_6);
}

/*****************************************************************************
* funcation name   : bluetooth_module_control_BRT_gpio_output
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
void  bluetooth_module_control_BRT_gpio_output( GPIO_LEVEL_ENUM on_off )
{
    if ( on_off == GPIO_HIGH )
    {
         GPIO_SetBits(GPIOA,GPIO_Pin_12);  
    }
    else
    {
        GPIO_ResetBits(GPIOA,GPIO_Pin_12);  
    }
}
/*****************************************************************************
* funcation name   : bluetooth_module_control_BT_CTL_gpio_output
*
* input  :  high => shut down the bluetooth module
*               low => start broadcast ,and connect device
*
* Output  :
* 
* Return Value :  none
* 
* descryption: shut down or turn on the bluetooth module
*
*****************************************************************************/
void  bluetooth_module_control_BT_CTL_gpio_output( GPIO_LEVEL_ENUM on_off )
{
    if ( on_off == GPIO_HIGH)
    {
         GPIO_SetBits(GPIOC,GPIO_Pin_8);  
    }
    else
    {
        GPIO_ResetBits(GPIOC,GPIO_Pin_8);  
    }
}
/*****************************************************************************
* funcation name   : bluetooth_module_control_MODE_CTL_gpio_output
*
* input  : low => command mode
*               high => throught mode
*
* Output  :
* 
* Return Value : 
* 
* descryption: switch the command mode and throught mode
*
*****************************************************************************/
void  bluetooth_module_control_MODE_CTL_gpio_output( GPIO_LEVEL_ENUM on_off )
{
    if ( on_off == GPIO_HIGH )
    {
         GPIO_SetBits(GPIOA,GPIO_Pin_8);  
    }
    else
    {
        GPIO_ResetBits(GPIOA,GPIO_Pin_8);  
    }
}

/*****************************************************************************
* funcation name   : bluetooth_module_read_BRE_gpio_input
*
* input  : 
*
* Output  :
* 
* Return Value :  high => uart is ok to receive data
*                             low => uart is not ready to receive data
* 
* descryption:
*
*****************************************************************************/
u8  bluetooth_module_read_BRE_gpio_input( void )
{
    return GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_9);
}

/*****************************************************************************
* funcation name   : bluetooth_module_read_BTT_gpio_input
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
u8  bluetooth_module_read_BTT_gpio_input( void )
{
    return GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_7);
}


/*****************************************************************************
* funcation name   : bluetooth_module_read_CONN_START_gpio_input
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
u8  bluetooth_module_read_CONN_START_gpio_input( void )
{
    return GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_11);
}

