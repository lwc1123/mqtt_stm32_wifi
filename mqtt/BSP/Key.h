#ifndef __KEY_H
#define __KEY_H	 

typedef enum GPIO_LEVEL
{
    GPIO_LOW =0,
    GPIO_HIGH,
}GPIO_LEVEL_ENUM;

#define KEY0 GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_13)   //读PA13
#define KEY1 GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_15)	//读PA15 
#define KEY2 GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0)	//读PA0，默认高电平
#define KEY_BROKEN GPIO_ReadInputDataBit(GPIOB,GPIO_Pin_3)	//读PB3，默认高电平

void Key_GPIO_Config(void);//IO初始化
s8 KEY_Scan(void);  //按键扫描函数
u8 mqtt_pub_key_scan(void);


void  bluetooth_module_reset( void );
void  bluetooth_module_gpio_config( void );
void  bluetooth_module_control_BRT_gpio_output( GPIO_LEVEL_ENUM on_off );
void  bluetooth_module_control_BT_CTL_gpio_output( GPIO_LEVEL_ENUM on_off );
void  bluetooth_module_control_MODE_CTL_gpio_output( GPIO_LEVEL_ENUM on_off );
u8  bluetooth_module_read_BRE_gpio_input( void );
u8  bluetooth_module_read_BTT_gpio_input( void );
u8  bluetooth_module_read_CONN_START_gpio_input( void );

void  wifi_module_reset( void );
void  wifi_module_gpio_config( void );

#endif
