/******************************************************************************

  Copyright (C), 2001-2011, CSST Co., Ltd.

 ******************************************************************************
  File Name     : delay.c
  
  Version       : Initial Draft
  
  Author        : kingerzhou
  
  Created       : 2015/11/16

  Last Modified :
  
  Description   : just delay time to occupy CPU
******************************************************************************/
#include "delay.h"
/*****************************************************************************
* funcation name   : delay_ms
*
* input  :  time => ms
*
* Output  :
* 
* Return Value : void
* 
* descryption: delay ms and occupy the CPU
*
*****************************************************************************/
#include "includes.h"

#define COM_START       "+++"


#define TIMER2_OUTTIME_INTERRUPT        1// s


extern unsigned long MilliTimer;
u16 delay_time=0;
void RTC_set_time(u32 time);

void delay_ms(u16 time)
{    
   delay_time = time;
   while(delay_time != 0);
}


void timer2_NVIC_configuration(void)
{
        NVIC_InitTypeDef NVIC_InitStructure;
        
        NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);	
        NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn; 
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0; 
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3; 
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
        NVIC_Init(&NVIC_InitStructure);  
}

void timer3_NVIC_configuration(void)
{
        NVIC_InitTypeDef NVIC_InitStructure;
        
        NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);	
        NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn; 
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0; 
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2; 
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
        NVIC_Init(&NVIC_InitStructure);  
}


void RTC_NVIC_Configuration(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	
	/* Configure one bit for preemption priority */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	
	/* Enable the RTC Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void RTC_Configuration(void)
{
	/* Enable PWR and BKP clocks */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
	
	/* Allow access to BKP Domain */
	PWR_BackupAccessCmd(ENABLE);
	
	/* Reset Backup Domain */
	BKP_DeInit();
	
	/* Enable LSE */
	RCC_LSEConfig(RCC_LSE_ON);
	/* Wait till LSE is ready */
	while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
	{}
	
	/* Select LSE as RTC Clock Source */
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
	
	/* Enable RTC Clock */
	RCC_RTCCLKCmd(ENABLE);
	
	/* Wait for RTC registers synchronization */
	RTC_WaitForSynchro();
	
	/* Wait until last write operation on RTC registers has finished */
	RTC_WaitForLastTask();
	
	/* Enable the RTC Second */
	RTC_ITConfig(RTC_IT_SEC, ENABLE);
	
	/* Wait until last write operation on RTC registers has finished */
	RTC_WaitForLastTask();
	
	/* Set RTC prescaler: set RTC period to 1sec */
	RTC_SetPrescaler(32767); /* RTC period = RTCCLK/RTC_PR = (32.768 KHz)/(32767+1) */
	
	/* Wait until last write operation on RTC registers has finished */
	RTC_WaitForLastTask();
}

void RTC_int(void)
{
    if (BKP_ReadBackupRegister(BKP_DR1) != 0xA5A5)
    {
        RTC_Configuration();
        BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
    }
    else
    {
        RTC_WaitForSynchro();  
        RTC_WaitForLastTask();  
        RTC_ITConfig(RTC_IT_SEC, ENABLE);  
        RTC_WaitForLastTask();  
    }

        /* Clear reset flags */
        RCC_ClearFlag();
}

void RTC_set_time(u32 time)
{
    PWR_BackupAccessCmd(ENABLE);
    RTC_WaitForLastTask();
    RTC_SetCounter(time);
    RTC_WaitForLastTask();
    BKP_WriteBackupRegister(BKP_DR1, 0xA5A5);
}

/*****************************************************************************

* funcation name   : timer2_init
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption: initialize the timer2,and make it work
*
* ((1+TIM_Prescaler)/72M)*(1+TIM_Period)=((1+7199)/72M)*(1+9999)=1��
*
*****************************************************************************/
void  timer2_init( void ) // 1s
{
        TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

        TIM_DeInit(TIM2); 
        
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE); 
        TIM_TimeBaseStructure.TIM_Period = (10000*TIMER2_OUTTIME_INTERRUPT-1); 
        //TIM_TimeBaseStructure.TIM_Period = (5000*TIMER2_OUTTIME_INTERRUPT-1);
        TIM_TimeBaseStructure.TIM_Prescaler =(7200-1);
        TIM_TimeBaseStructure.TIM_ClockDivision = 0; 
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
        TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); 
        TIM_ClearFlag(TIM2,TIM_FLAG_Update); 
        TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
        
        TIM_Cmd(TIM2, ENABLE); 
}
/*****************************************************************************

* funcation name   : timer3_init
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption: initialize the timer2,and make it work
*
* ((1+TIM_Prescaler)/72M)*(1+TIM_Period)=((1+7199)/72M)*(1+9999)=1��
*
*****************************************************************************/
void  timer3_init( void ) // 1ms
{
        TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

        TIM_DeInit(TIM3); 
        
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); 
        TIM_TimeBaseStructure.TIM_Period = (10-1); 
        TIM_TimeBaseStructure.TIM_Prescaler =(7200-1);
        TIM_TimeBaseStructure.TIM_ClockDivision = 0; 
        TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
        TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); 
        TIM_ClearFlag(TIM3,TIM_FLAG_Update); 
        TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
        
        TIM_Cmd(TIM3, ENABLE); 
}

/*****************************************************************************
* funcation name   : TIM2_IRQHandler
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption: timer2 ISR
*
*****************************************************************************/
void TIM2_IRQHandler(void)
{
    static u8 count=0;

    if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM2 , TIM_FLAG_Update);
        //securety_box_send_heart_beat_sync();
         //test_array();
         
        //uart3_send_data("wifi test",9);
      //  D_PRINTF("motor\r\n");
      //  motor_counter_clock_wise_run_with_pulse(400);
        if(count == 5)
        {
            //RTC_set_time(0x1221);
            //uart1_send_data("blue test",9);
            count=0;
        }
        count++;
      // D_PRINTF("time:%d\r\n", RTC_GetCounter());
    }
}
/*****************************************************************************
* funcation name   : TIM3_IRQHandler
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
void  TIM3_IRQHandler( void )
{
     if(TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM3, TIM_FLAG_Update);
        MilliTimer++;
        if (delay_time != 0x00)
        { 
            delay_time--;
        }
   }
}

/*****************************************************************************
* funcation name   : RTC_IRQHandler
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption: RTC ISR
*
*****************************************************************************/

void RTC_IRQHandler(void)  
{  
  if (RTC_GetITStatus(RTC_IT_SEC) != RESET)  
  {  
    RTC_WaitForLastTask();  
  
    /* Clear the RTC Second interrupt */  
    RTC_ClearITPendingBit(RTC_IT_SEC);  
    //D_PRINTF("RTC ISR\r\n");
    /* Wait until last write operation on RTC registers has finished */  
    RTC_WaitForLastTask();  
      
  }  
}  


