/******************************************************************************

  Copyright (C), 2001-2011, CSST  Co., Ltd.

 ******************************************************************************
  File Name     : i2c.c
  
  Version       : Initial Draft
  
  Author        : kingerzhou
  
  Created       : 2015/11/6
  
  Last Modified :
  
  Description   : this module is supply I2C interface for another module
  
******************************************************************************/
#include "includes.h"




#define I2C_Speed 400000
#define MASTER_DEVICE_ADDRESS       0x0a


/*****************************************************************************
* funcation name   : i2c_gpio_config
*
* input  : I2C_BUS_ENUM => decide which i2c bus , i2c_bus1 or i2c_bus2
*
* Output  : void
* 
* Return Value : void
* 
* descryption: config gpio for i2c ,and which i2c bus to use.
*                        There are two i2c bus, i2c_bus1 and i2c_bus2
*
*****************************************************************************/

 void  i2c_gpio_config( I2C_BUS_ENUM i2c_bus )
{
     GPIO_InitTypeDef  GPIO_InitStructure; 

    if ( I2C_BUS1 == i2c_bus )
    {
            /* enable I2C_1 bus clock */
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1,ENABLE);  

        /* PB6-I2C1_SCL��PB7-I2C1_SDA*/
        GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6 | GPIO_Pin_7;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;	       
        GPIO_Init(GPIOB, &GPIO_InitStructure);
    }
    else
    {
            /* enable I2C_2 bus clock */
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
        RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1,ENABLE);  

        /* PB10-I2C2_SCL��PB11-I2C2_SDA*/
        GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_10 | GPIO_Pin_11;
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
        GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_OD;	       
        GPIO_Init(GPIOB, &GPIO_InitStructure);
    }

}


/*****************************************************************************
* funcation name   : i2c_mode_config
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
 void  i2c_mode_config( I2C_BUS_ENUM i2c_bus )
{
    I2C_InitTypeDef  I2C_InitStructure; 
    
    if ( I2C_BUS1 == i2c_bus )
    {
      /* i2c work mode setting */
      I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
      I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
      I2C_InitStructure.I2C_OwnAddress1 =MASTER_DEVICE_ADDRESS; 
      I2C_InitStructure.I2C_Ack = I2C_Ack_Enable ;
      I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
      I2C_InitStructure.I2C_ClockSpeed = I2C_Speed;
      
      I2C_Cmd(I2C1, ENABLE);
      I2C_Init(I2C1, &I2C_InitStructure); 
    }
    else
    {
      /* i2c work mode setting */
      I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
      I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
      I2C_InitStructure.I2C_OwnAddress1 =MASTER_DEVICE_ADDRESS; 
      I2C_InitStructure.I2C_Ack = I2C_Ack_Enable ;
      I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
      I2C_InitStructure.I2C_ClockSpeed = I2C_Speed;
      
      I2C_Cmd(I2C2, ENABLE);
      I2C_Init(I2C2, &I2C_InitStructure); 
    }

}
/*****************************************************************************
* funcation name   : i2c_bus1_write_bytes
*
* input  :  devaddr => slave device address 
*               subaddr => slave device sub address
*               buffer =>  which store datas is to write
*               length => length of storing datas
* Output  : none
* 
* Return Value : void
* 
* descryption: write datas to slave device by i2c bus1
*
*****************************************************************************/
void  i2c_bus1_write_bytes(u8 devaddr, u8 subaddr, u8 *buffer,u8 length)
{

  /* the i2c bus is free */
  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY)); 
    
  /* Send START condition */
  I2C_GenerateSTART(I2C1, ENABLE);
  
  /* Test on EV5 and clear it */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT)); 
  
  /* Send device address for write */
  I2C_Send7bitAddress(I2C1, devaddr, I2C_Direction_Transmitter);
  
  /* Test on EV6 and clear it */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));  

  /* Send the slave device address to write to */    
  I2C_SendData(I2C1, subaddr);  

  /* Test on EV8 and clear it */
  while(! I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

  /* While there is data to be written */
  while(length--)  
  {
    /* Send the current byte */
    I2C_SendData(I2C1, *buffer); 

    /* Point to the next byte to be written */
    buffer++; 
  
    /* Test on EV8 and clear it */
    while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  /* Send STOP condition */
  I2C_GenerateSTOP(I2C1, ENABLE);
}
/*****************************************************************************
* funcation name   : i2c_bus1_read_bytes
*
* input  :  devaddr => slave device address 
*               subaddr => slave device sub address
*               buffer =>  which store datas is to read
*               length => how many datas are to read
* Output  : none
* 
* Return Value : 
* 
* descryption: read datas from slave device by i2c bus1
*
*****************************************************************************/
void i2c_bus1_read_bytes(u8 devaddr,u8 subaddr,u8 *buffer, u8 length)
{  

  while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY)); 
    
    
  /* Send START condition */
  I2C_GenerateSTART(I2C1, ENABLE);
  
  /* Test on EV5 and clear it */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));

  /* Send slave device address for write */
  I2C_Send7bitAddress(I2C1, devaddr, I2C_Direction_Transmitter);

  /* Test on EV6 and clear it */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
  
  /* Clear EV6 by setting again the PE bit */
  I2C_Cmd(I2C1, ENABLE);

  /* Send the slave sub address to write to */
  I2C_SendData(I2C1, subaddr);  

  /* Test on EV8 and clear it */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  
  /* Send STRAT condition a second time */  
  I2C_GenerateSTART(I2C1, ENABLE);
  
  /* Test on EV5 and clear it */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT));
  
  /* Send slave address for read */
  I2C_Send7bitAddress(I2C1, devaddr, I2C_Direction_Receiver);
  
  /* Test on EV6 and clear it */
  while(!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
  
  /* While there is data to be read */
  while(length)  
  {
    if(length == 1)
    {
      /* Disable Acknowledgement */
      I2C_AcknowledgeConfig(I2C1, DISABLE);
      
      /* Send STOP Condition */
      I2C_GenerateSTOP(I2C1, ENABLE);
    }

    /* Test on EV7 and clear it */
    if(I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED))  
    {      
      /* Read a byte from the slave device */
      *buffer = I2C_ReceiveData(I2C1);

      /* Point to the next location where the byte read will be saved */
      buffer++; 
      
      /* Decrement the read bytes counter */
      length--;        
    }   
  }

  /* Enable Acknowledgement to be ready for another reception */
  I2C_AcknowledgeConfig(I2C1, ENABLE);
}

/*****************************************************************************
* funcation name   : i2c_bus2_write_bytes
*
* input  :  devaddr => slave device address 
*               subaddr => slave device sub address
*               buffer =>  which store datas is to write
*               length => length of storing datas
* Output  : none
* 
* Return Value : void
* 
* descryption: write datas to slave device by i2c bus2
*
*****************************************************************************/
void  i2c_bus2_write_bytes(u8 devaddr, u8 subaddr, u8 *buffer,u8 length)
{

  /* the i2c bus is free */
  while(I2C_GetFlagStatus(I2C2, I2C_FLAG_BUSY)); 
    
  /* Send START condition */
  I2C_GenerateSTART(I2C2, ENABLE);
  
  /* Test on EV5 and clear it */
  while(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT)); 
  
  /* Send device address for write */
  I2C_Send7bitAddress(I2C2, devaddr, I2C_Direction_Transmitter);
  
  /* Test on EV6 and clear it */
  while(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));  

  /* Send the slave device address to write to */    
  I2C_SendData(I2C2, subaddr);  

  /* Test on EV8 and clear it */
  while(! I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));

  /* While there is data to be written */
  while(length--)  
  {
    /* Send the current byte */
    I2C_SendData(I2C2, *buffer); 

    /* Point to the next byte to be written */
    buffer++; 
  
    /* Test on EV8 and clear it */
    while (!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  }
  /* Send STOP condition */
  I2C_GenerateSTOP(I2C2, ENABLE);
}
/*****************************************************************************
* funcation name   : i2c_bus2_read_bytes
*
* input  :  devaddr => slave device address 
*               subaddr => slave device sub address
*               buffer =>  which store datas is to read
*               length => how many datas are to read
* Output  : none
* 
* Return Value : 
* 
* descryption: read datas from slave device by i2c bus2
*
*****************************************************************************/
void i2c_bus2_read_bytes(u8 devaddr,u8 subaddr,u8 *buffer , u8 length)
{  

  while(I2C_GetFlagStatus(I2C2, I2C_FLAG_BUSY)); 
    
    
  /* Send START condition */
  I2C_GenerateSTART(I2C2, ENABLE);
  
  /* Test on EV5 and clear it */
  while(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT));

  /* Send slave device address for write */
  I2C_Send7bitAddress(I2C2, devaddr, I2C_Direction_Transmitter);

  /* Test on EV6 and clear it */
  while(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
  
  /* Clear EV6 by setting again the PE bit */
  I2C_Cmd(I2C2, ENABLE);

  /* Send the slave sub address to write to */
  I2C_SendData(I2C2, subaddr);  

  /* Test on EV8 and clear it */
  while(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
  
  /* Send STRAT condition a second time */  
  I2C_GenerateSTART(I2C2, ENABLE);
  
  /* Test on EV5 and clear it */
  while(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_MODE_SELECT));
  
  /* Send slave address for read */
  I2C_Send7bitAddress(I2C2, devaddr, I2C_Direction_Receiver);
  
  /* Test on EV6 and clear it */
  while(!I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
  
  /* While there is data to be read */
  while(length)  
  {
    if(length == 1)
    {
      /* Disable Acknowledgement */
      I2C_AcknowledgeConfig(I2C2, DISABLE);
      
      /* Send STOP Condition */
      I2C_GenerateSTOP(I2C2, ENABLE);
    }

    /* Test on EV7 and clear it */
    if(I2C_CheckEvent(I2C2, I2C_EVENT_MASTER_BYTE_RECEIVED))  
    {      
      /* Read a byte from the slave device */
      *buffer = I2C_ReceiveData(I2C2);

      /* Point to the next location where the byte read will be saved */
      buffer++; 
      
      /* Decrement the read bytes counter */
      length--;        
    }   
  }

  /* Enable Acknowledgement to be ready for another reception */
  I2C_AcknowledgeConfig(I2C2, ENABLE);
}



