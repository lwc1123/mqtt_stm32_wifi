#ifndef __UART_H
#define	__UART_H

#include "stm32f10x.h"
#include<stdio.h>


#define DEBUG
#ifdef DEBUG
#define D_PRINTF(fmt,args...)   printf(fmt,##args)
#else
#define D_PRINTF(fmt,args...)
#endif

#define UART3_DMA_BUFFER_SIZE 256
#define UART1_DMA_BUFFER_SIZE 64




typedef struct SNED_DATA{
u8 buffer[UART3_DMA_BUFFER_SIZE];
u8 length;
}SEND_DATA;

typedef struct RECEIVE_DATA{
u8 buffer[UART3_DMA_BUFFER_SIZE];
u8 length;
}RECEIVE_DATA;

typedef struct UART1_SNED_DATA{
u8 buffer[UART1_DMA_BUFFER_SIZE];
u8 length;
}UART1_SEND_DATA;

typedef struct UART1_RECEIVE_DATA{
u8 buffer[UART1_DMA_BUFFER_SIZE];
u8 length;
}UART1_RECEIVE_DATA;



void USART1_Config(void);

void  uart1_config_init( int baudrate );
void  uart2_config_init( int baudrate );
void  uart3_config_init( int baudrate );

u8  uart1_send_data( u8 *data,u8 length);

u8  uart3_receive_data( u8 *data);
u8  uart3_send_data( u8 *data,u8 length);


int fputc(int ch, FILE *f);
int GetKey (void);

#endif /* __USART1_H */
