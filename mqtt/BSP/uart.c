/******************************************************************************

 Copyright (C), All rights resolved by CSST company, This is unpulished.

 ******************************************************************************
  File Name     : uart.c
  
  Version       : Initial Draft
  
  Author        : kingerzhou
  
  Created       : 2015/11/4
  
  Last Modified :
  
  Description   : supply interface of uart
  
******************************************************************************/
#include"includes.h"
#include "uart.h"
#include <stdarg.h>
#include<string.h>

/* use different uart to printf */
#define PRINTF_UART     USART2



SEND_DATA uart3_sdata={0};
RECEIVE_DATA uart3_rdata={0};
UART1_SEND_DATA uart1_sdata={0};
UART1_RECEIVE_DATA uart1_rdata={0};



u8 g_uart_send_flag =1; //no send complete
u8 g_uart_receive_flag =1; // no received

/* declar funcation */
void DMA_uart1_Init(void)  ;
void DMA_uart3_Init(void)  ;
void uart3_DMA_receive_data(void);


/*****************************************************************************
* funcation name   : uart2_NVIC_configuration
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
void  uart1_NVIC_configuration( void )
{
        NVIC_InitTypeDef NVIC_InitStructure; 

        NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4); //
        NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;	 
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority =3; //lower than uart3
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&NVIC_InitStructure);
}

/*****************************************************************************
* funcation name   : uart1_config_init
*
* input  : baudrate =>  uart interface baudrate
*
* Output  : none
* 
* Return Value : void
* 
* descryption: initialize the uart1 with baudrate,and setting is 8 bits data,stop bits is 1,no parity.
*                       no hardware flow control
*
*****************************************************************************/
void  uart1_config_init( int baudrate )
{
    
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
         NVIC_InitTypeDef NVIC_InitStructure; 
         
	/* all gpios and uart1 connect to APB2 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 , ENABLE);
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE);

             /* Enable the DMA Interrupt */
         NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);  
         NVIC_InitStructure.NVIC_IRQChannel= DMA1_Channel5_IRQn;   
         NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority= 1;   
         NVIC_InitStructure.NVIC_IRQChannelSubPriority= 1;   
         NVIC_InitStructure.NVIC_IRQChannelCmd= ENABLE;   
         NVIC_Init(&NVIC_InitStructure);   

         /* Enable the uart1 Interrupt */

         NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;	 
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
    
	/* USART1 GPIO config */
	/* Configure USART1 Tx (PA.09) as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);    
	/* Configure USART1 Rx (PA.10) as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

         // uart1_NVIC_configuration();
          DMA_uart1_Init();
	/* uart1 property setting*/
	USART_InitStructure.USART_BaudRate = baudrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure); 

        USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); 

        /* Enable USART1 Receive and Transmit interrupts */
        USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);  //enable uart idle interrupts
        
        USART_Cmd(USART1, ENABLE);

        /* Enable USARTy DMA TX request */
        //USART_DMACmd(USART3, USART_DMAReq_Tx, ENABLE);  // enable uart DMA tramsmit
        USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE); // enable uart DMA receive


}



/*****************************************************************************
* funcation name   : uart2_config_init
*
* input  : baudrate =>  uart interface baudrate
*
* Output  : none
* 
* Return Value : void
* 
* descryption: initialize the uart2 with baudrate,and setting is 8 bits data,stop bits is 1,no parity.
*                       no hardware flow control
*
*****************************************************************************/
void  uart2_config_init( int baudrate )
{
    
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

        /* all gpios  connect to APB1,but uart2 connect to APB2*/
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2 , ENABLE);
         RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE);
	
	/* USART2 GPIO config */
	/* Configure USART2 Tx (PA.02) as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);    
	/* Configure USART2 Rx (PA.03) as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	  
	/* uart2 setting*/
	USART_InitStructure.USART_BaudRate = baudrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART2, &USART_InitStructure); 
	USART_Cmd(USART2, ENABLE);
}



/*****************************************************************************
* funcation name   : uart3_config_init
*
* input  : baudrate =>  uart interface baudrate
*
* Output  : none
* 
* Return Value : void
* 
* descryption: initialize the uart3 with baudrate,and setting is 8 bits data,stop bits is 1,no parity.
*                       no hardware flow control
*
*****************************************************************************/
void  uart3_config_init( int baudrate )
{
    
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
         NVIC_InitTypeDef NVIC_InitStructure; 
	
	    /* all gpios  connect to APB1,but uart3 connect to APB2*/
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3 , ENABLE);
         RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOB, ENABLE);


         /* Enable the DMA Interrupt */
         NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  
         NVIC_InitStructure.NVIC_IRQChannel= DMA1_Channel2_IRQn;   //UART3_TX => DMA1_channel2
         NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority= 2;   
         NVIC_InitStructure.NVIC_IRQChannelSubPriority= 1;   
         NVIC_InitStructure.NVIC_IRQChannelCmd= ENABLE;   
         NVIC_Init(&NVIC_InitStructure);   

      /* Enable the uart3 Interrupt */
	
	NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;	 
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* USART3 GPIO config */
	/* Configure USART3Tx (PB.10) as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);    
	/* Configure USART3 Rx (PB.11) as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

         DMA_uart3_Init();   // setting uart and MDA
	/* uart3setting*/
	USART_InitStructure.USART_BaudRate = baudrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

        USART_Init(USART3, &USART_InitStructure); 
        
         /* Enable USART3 Receive and Transmit interrupts */
        USART_ITConfig(USART3, USART_IT_IDLE, ENABLE);  //enable uart idle interrupts
        
        USART_Cmd(USART3, ENABLE);

        /* Enable USARTy DMA TX request */
        USART_DMACmd(USART3, USART_DMAReq_Tx, ENABLE);  // enable uart DMA tramsmit
        USART_DMACmd(USART3, USART_DMAReq_Rx, ENABLE); // enable uart DMA receive


}

void USART1_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	
	/* 配置USART1时钟*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);
	
	/* USART1 GPIO config */
	/* Configure USART1 Tx (PA.09) as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);    
	/* Configure USART1 Rx (PA.10) as input floating */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	  
	/* USART1 工作模式配置*/
	USART_InitStructure.USART_BaudRate = 9600;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure); 
	USART_Cmd(USART1, ENABLE);
}
/*****************************************************************************
* funcation name   : DMA_uart1_Init
*
* input  : none
*
* Output  : none
* 
* Return Value : void
* 
* descryption: initialize DMA,and make DMA contact with uart1
*
*****************************************************************************/
void DMA_uart1_Init(void)  
{  
        DMA_InitTypeDef  DMA_InitStructure;  

        RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);  
        
        /* DMA channel configuration */  
        DMA_Cmd(DMA1_Channel5, DISABLE);
        DMA_DeInit(DMA1_Channel5);  
        
        /*destine address which is DMA send to */
        DMA_InitStructure.DMA_PeripheralBaseAddr       = (u32)(&USART1->DR);  //uart1 base address
        /* source address which DMA get datas */
        DMA_InitStructure.DMA_MemoryBaseAddr           =(u32)uart1_rdata.buffer;  //source data address
        DMA_InitStructure.DMA_DIR                                      =DMA_DIR_PeripheralSRC;   //desterny to source
        DMA_InitStructure.DMA_BufferSize                          =UART1_DMA_BUFFER_SIZE; //DMA channel buffer size
        DMA_InitStructure.DMA_PeripheralInc                   =DMA_PeripheralInc_Disable; 
        DMA_InitStructure.DMA_MemoryInc                       =DMA_MemoryInc_Enable;   
        DMA_InitStructure.DMA_PeripheralDataSize         = DMA_PeripheralDataSize_Byte;
        DMA_InitStructure.DMA_MemoryDataSize            =DMA_PeripheralDataSize_Byte;  
        DMA_InitStructure.DMA_Mode                                  =DMA_Mode_Normal;  
        DMA_InitStructure.DMA_Priority                      =DMA_Priority_High;  
        DMA_InitStructure.DMA_M2M                                   =DMA_M2M_Disable; //disable memory to memory
        DMA_Init(DMA1_Channel5,&DMA_InitStructure);  

        DMA_ClearFlag(DMA1_FLAG_GL5);   //clear all flags
        DMA_Cmd(DMA1_Channel5, ENABLE); 

}

/*****************************************************************************
* funcation name   : DMA_uart_Init
*
* input  : none
*
* Output  : none
* 
* Return Value : void
* 
* descryption: initialize DMA,and make DMA contact with uart3
*
*****************************************************************************/
void DMA_uart3_Init(void)  
{  
        DMA_InitTypeDef  DMA_InitStructure;  

        RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);  
        
        /* DMA channel configuration */  
        DMA_Cmd(DMA1_Channel2, DISABLE);
        DMA_DeInit(DMA1_Channel2);  
        
        /*destine address which is DMA send to */
        DMA_InitStructure.DMA_PeripheralBaseAddr       = (u32)(&USART3->DR);  //uart3 base address
        /* source address which DMA get datas */
        DMA_InitStructure.DMA_MemoryBaseAddr                    =(u32)uart3_sdata.buffer;  //source data address
        DMA_InitStructure.DMA_DIR                                      =DMA_DIR_PeripheralDST;   //source to desterny
        DMA_InitStructure.DMA_BufferSize                          =UART3_DMA_BUFFER_SIZE; //DMA channel buffer size
        DMA_InitStructure.DMA_PeripheralInc                   =DMA_PeripheralInc_Disable; 
        DMA_InitStructure.DMA_MemoryInc                       =DMA_MemoryInc_Enable;   
        DMA_InitStructure.DMA_PeripheralDataSize         = DMA_PeripheralDataSize_Byte;
        DMA_InitStructure.DMA_MemoryDataSize            =DMA_PeripheralDataSize_Byte;  
        DMA_InitStructure.DMA_Mode                                  =DMA_Mode_Normal;  
        DMA_InitStructure.DMA_Priority                      =DMA_Priority_VeryHigh;  
        DMA_InitStructure.DMA_M2M                                   =DMA_M2M_Disable; //disable memory to memory
        DMA_Init(DMA1_Channel2,&DMA_InitStructure);  

        DMA_ClearFlag(DMA1_FLAG_GL2);   //clear all flags
        DMA_Cmd(DMA1_Channel2, ENABLE); 
        DMA_ITConfig(DMA1_Channel2, DMA_IT_TC, ENABLE);  //enable dma send interrupt

        
        /* DMA channel configuration */  
        DMA_Cmd(DMA1_Channel3, DISABLE);
        DMA_DeInit(DMA1_Channel3);  
        
        /*destine address which is DMA send to */
        DMA_InitStructure.DMA_PeripheralBaseAddr       = (u32)(&USART3->DR);  //uart3 base address
        /* source address which DMA get datas */
        DMA_InitStructure.DMA_MemoryBaseAddr           =(u32)uart3_rdata.buffer;  //source data address
        DMA_InitStructure.DMA_DIR                                      =DMA_DIR_PeripheralSRC;   //desterny to source
        DMA_InitStructure.DMA_BufferSize                          =UART3_DMA_BUFFER_SIZE; //DMA channel buffer size
        DMA_InitStructure.DMA_PeripheralInc                   =DMA_PeripheralInc_Disable; 
        DMA_InitStructure.DMA_MemoryInc                       =DMA_MemoryInc_Enable;   
        DMA_InitStructure.DMA_PeripheralDataSize         = DMA_PeripheralDataSize_Byte;
        DMA_InitStructure.DMA_MemoryDataSize            =DMA_PeripheralDataSize_Byte;  
        DMA_InitStructure.DMA_Mode                                  =DMA_Mode_Normal;  
        DMA_InitStructure.DMA_Priority                      =DMA_Priority_VeryHigh;  
        DMA_InitStructure.DMA_M2M                                   =DMA_M2M_Disable; //disable memory to memory
        DMA_Init(DMA1_Channel3,&DMA_InitStructure);  

        DMA_ClearFlag(DMA1_FLAG_GL3);   //clear all flags
        DMA_Cmd(DMA1_Channel3, ENABLE); 

}
/*****************************************************************************
* funcation name   : DMA1_Channel2_IRQHandler
*
* input  : none
*
* Output  : none
* 
* Return Value : void
* 
* descryption: ISR when DMA have send data to uart3
*                        this is flag when data have sent
*
*****************************************************************************/
void DMA1_Channel2_IRQHandler(void)
{
    if(DMA_GetITStatus(DMA1_FLAG_TC2))
    {
        DMA_ClearFlag(DMA1_FLAG_GL2);         // clear all flags
        DMA_Cmd(DMA1_Channel2, DISABLE);   // disable dma channel2
      // D_PRINTF("DMA UART3 send Bytes completed\r\n");
       g_uart_send_flag =0; // uart send data completed
    }
}
/*****************************************************************************
* funcation name   : uart3_start_DMA_transmit_data
*
* input  : size => how many datas
*
* Output  : none
* 
* Return Value : void
* 
* descryption:  uart3 start to send data by DMA
*
*****************************************************************************/
void uart3_start_DMA_transmit_data(u8 size)
{
    DMA1_Channel2->CNDTR = (uint16_t)size; 
    DMA_Cmd(DMA1_Channel2, ENABLE);       
}

/*****************************************************************************
* funcation name   : USART1_IRQHandler
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption: bluetooth module use DMA uart1 interrupt
*
*****************************************************************************/
void  USART1_IRQHandler( void )
{
    /* receive interrupt */
    if(USART_GetITStatus(USART1, USART_IT_IDLE) != RESET)  
    {
        DMA_Cmd(DMA1_Channel5, DISABLE);       
        DMA_ClearFlag( DMA1_FLAG_GL5 );    
        uart1_rdata.length= UART1_DMA_BUFFER_SIZE - DMA_GetCurrDataCounter(DMA1_Channel5); 
        DMA1_Channel5->CNDTR = UART1_DMA_BUFFER_SIZE;   
        DMA_Cmd(DMA1_Channel5, ENABLE);
        //blue_rx_msg.address = (u32)uart1_rdata.buffer;
        //blue_rx_msg.length = uart1_rdata.length;
        //OSMboxPost(blue_rx_mbox,&blue_rx_msg);
        USART_ReceiveData( USART1 );
        D_PRINTF("uart1 receive ISR\r\n");
        #if 0
        {
            u8 i;
               D_PRINTF("\r\nblue recevie data[%d]:",uart1_rdata.length);
               for (i = 0 ; i < uart1_rdata.length; i++ )
               {
                   D_PRINTF("0x%02x ", uart1_rdata.buffer[i]);
               }
               D_PRINTF("\r\n");
         }
               #endif
    }
}

/*****************************************************************************
* funcation name   : USART3_IRQHandler
*
* input  :  none
*
* Output  : none
* 
* Return Value : void
* 
* descryption: when uart3 received data, generate ISR.
*
*****************************************************************************/
void USART3_IRQHandler(void)
{
    if(USART_GetITStatus(USART3, USART_IT_IDLE) != RESET)  
    {
        uart3_DMA_receive_data();
        USART_ReceiveData( USART3 ); // Clear IDLE interrupt flag bit
        //D_PRINTF("uart3 receive ISR\r\n");
      	g_uart_receive_flag = 0; // received uart data complete
    }
}

void uart3_DMA_receive_data(void)
{
    DMA_Cmd(DMA1_Channel3, DISABLE);       
    DMA_ClearFlag( DMA1_FLAG_GL3 );    
    uart3_rdata.length= UART3_DMA_BUFFER_SIZE - DMA_GetCurrDataCounter(DMA1_Channel3); 
    DMA1_Channel3->CNDTR = UART3_DMA_BUFFER_SIZE;   
    DMA_Cmd(DMA1_Channel3, ENABLE);
    wifirx_msg.address =(u32)uart3_rdata.buffer;
    wifirx_msg.length = uart3_rdata.length;
    //D_PRINTF("wifi rx len[%d]\r\n",wifirx_msg.length);
    OSQPost(network_receive_qm,  &wifirx_msg);
}

/*****************************************************************************
* funcation name   : uart1_send_data
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
u8  uart1_send_data( u8 *data,u8 length)
{
    u8 i=0;
    D_PRINTF("aurt1 send data:");
    for ( i = 0 ; i <length ; i++ )
    {
        D_PRINTF("0x%02x ", data[i]);
        USART_SendData(USART1, data[i]);
        while( USART_GetFlagStatus(USART1,USART_FLAG_TC)!= SET);
    }
    D_PRINTF("\r\n");

	return 0;
}

/*****************************************************************************
* funcation name   : uart3_receive_data
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
u8  uart1_receive_data( u8 *data)
{
    u8 ret=0;
    
        
   return ret;
}


/*****************************************************************************
* funcation name   : uart3_send_data
*
* input  :  *data => which store data
*               length => how many datas need to send
*
* Output  : none
* 
* Return Value : void
* 
* descryption: uart3 send data to
*                       confirm data is not override every time
*****************************************************************************/
u8  uart3_send_data( u8 *data,u8 length)
{
    memset(uart3_sdata.buffer,0,UART3_DMA_BUFFER_SIZE);
    memcpy(uart3_sdata.buffer,data,length);
    uart3_sdata.length=length;
    //D_PRINTF("send:%s length:%d\r\n",uart_send_data.buffer,length);   
    uart3_start_DMA_transmit_data(length);
    while(g_uart_send_flag) //ture as data is sending...
    {
    	OSTimeDly(50);
    }// better to  use mbox?
	#if 0
    D_PRINTF("\r\nwifi send data:");
    for ( i = 0 ; i < uart3_sdata.length; i++ )
    {
        D_PRINTF("0x%02x ", uart3_sdata.buffer[i]);
    }
    D_PRINTF("\r\n");
    #endif
    
    return 0;
}

/*****************************************************************************
* funcation name   : uart3_receive_data
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
u8  uart3_receive_data( u8 *data)
{
    u8 ret=0;
    if(g_uart_receive_flag == 0) // received data from uart3
    {
        DMA_Cmd(DMA1_Channel3, DISABLE);       
        DMA_ClearFlag( DMA1_FLAG_GL3 ); 
        uart3_rdata.length= UART3_DMA_BUFFER_SIZE - DMA_GetCurrDataCounter(DMA1_Channel3); 
        DMA1_Channel3->CNDTR = UART3_DMA_BUFFER_SIZE;   
        DMA_Cmd(DMA1_Channel3, ENABLE);    
        g_uart_receive_flag =1; // no received data flag
        memcpy(data,&uart3_rdata.buffer, uart3_rdata.length);
        ret =  uart3_rdata.length;
    }
    return ret;
}
/*****************************************************************************
* funcation name   : fputc
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption: use uart as debug, output debug information on PC
*
*****************************************************************************/
int fputc(int ch, FILE *f)
{
	/* 将Printf内容发往串口 */
	USART_SendData(PRINTF_UART, (unsigned char) ch);
	//while( USART_GetFlagStatus(USART1,USART_FLAG_TC)!= SET);
         while (!(PRINTF_UART->SR & USART_FLAG_TXE));
	return (ch);
}

int GetKey (void)  {
 while (!(PRINTF_UART->SR & USART_FLAG_RXNE));
 return ((int)(PRINTF_UART->DR & 0x1FF));
}



