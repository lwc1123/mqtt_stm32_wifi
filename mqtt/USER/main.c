/****************************************************************
*copyright,All Rights Reserved by CSST company. This is unpulished.
*
*file:  main.c
*
*description: The  entry of whole project pragrammed.
*
*version:
*
*author:    kingerzhou
*
*last modified:
******************************************************************/
#include "includes.h"


/*******************设置任务优先级*******************/
#define STARTUP_TASK_PRIO      13   
#define LED0_TASK_Prio       12
#define KEY_TASK_Prio      2


#define TASK_BLUETOOTH_PRIO        6
#define TASK_WIFI_SEND_PRIO       8
#define TASK_WIFI_RECEIVE_PRIO      9
#define TASK_MQTT_SUB_PRIO       10
#define TASK_MQTT_PUB_PRIO      5






 

/************设置栈大小（单位为 OS_STK ）************/
#define STARTUP_TASK_STK_SIZE   128   
#define LED_STK_SIZE     64
#define KEY_STK_SIZE     64

#define TASK_BLUETOOTH_SIZE         256
#define TASK_WIFI_SEND_SIZE        256
#define TASK_WIFI_RECEIVE_SIZE       256
#define TASK_MQTT_SUB_SIZE           256
#define TASK_MQTT_PUB_SIZE           256


/************任务堆栈************/

static OS_STK startup_task_stk[STARTUP_TASK_STK_SIZE];		
//static OS_STK  TASK_KEY_STK[LED_STK_SIZE];
//static OS_STK  TASK_LED0_STK[LED_STK_SIZE];

static OS_STK TASK_WIFI_SEND_STACK[TASK_WIFI_SEND_SIZE];
static OS_STK TASK_WIFI_RECEIVED_STACK[TASK_WIFI_RECEIVE_SIZE];
static OS_STK TASK_MQTT_SUB_STACK[TASK_MQTT_SUB_SIZE];
static OS_STK TASK_MQTT_PUB_STACK[TASK_MQTT_PUB_SIZE];



/************任务声明************/
void Task_start(void *p_arg);
extern OS_EVENT *sem_mqttread;
  
int main(void)
{
    BSP_Init();
    OSInit();
    //create_all_module_tasks();
    OSTaskCreate(Task_start,(void *)0,&startup_task_stk[STARTUP_TASK_STK_SIZE-1], STARTUP_TASK_PRIO);
    OSStart();
    return 0;
 }
/*****************************************************************************
* funcation name   : Task_start
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption: create all module tasks
*
*****************************************************************************/
void Task_start(void *p_arg)
{
        OS_CPU_SR  cpu_sr;
        u8 ret;
        (void)p_arg;                		//
        OS_ENTER_CRITICAL();   
        create_network_send_message_queue_between_tasks();
        create_network_receive_message_queue_between_tasks();
        create_mqtt_sub_message_queue_between_tasks();
        create_mqtt_pub_message_queue_between_tasks();
        
        ret = OSTaskCreate(task_of_wifi_send_module_create, (void * )0, (OS_STK *)&TASK_WIFI_SEND_STACK[TASK_WIFI_SEND_SIZE-1], TASK_WIFI_SEND_PRIO);
        if( ret !=OS_ERR_NONE)
        {
            D_PRINTF("ERR: create wifi task\n");
        }
        ret = OSTaskCreate(task_of_wifi_receive_module_create, (void * )0, (OS_STK *)&TASK_WIFI_RECEIVED_STACK[TASK_WIFI_RECEIVE_SIZE-1], TASK_WIFI_RECEIVE_PRIO);
        if( ret !=OS_ERR_NONE)
        {
            D_PRINTF("ERR: create wifi task\n");
        }

		sem_mqttread = OSMutexCreate(1,&ret);
		if( ret != OS_ERR_NONE)
        {
            D_PRINTF("ERR: create Mutex\n");
        }

        ret = OSTaskCreate(task_of_mqtt_sub_module_create, (void * )0, (OS_STK *)&TASK_MQTT_SUB_STACK[TASK_MQTT_SUB_SIZE-1], TASK_MQTT_SUB_PRIO);
        if( ret !=OS_ERR_NONE)
        {
            D_PRINTF("ERR: create mqtt_sub task\n");
        }
        
        ret = OSTaskCreate(task_of_mqtt_pub_module_create, (void * )0, (OS_STK *)&TASK_MQTT_PUB_STACK[TASK_MQTT_PUB_SIZE-1], TASK_MQTT_PUB_PRIO);
        if( ret !=OS_ERR_NONE)
        {
            D_PRINTF("ERR: create mqtt_pub task\n");
        }
        OSTaskSuspend(STARTUP_TASK_PRIO);	//suspend but not delete
        OS_EXIT_CRITICAL();	
}


