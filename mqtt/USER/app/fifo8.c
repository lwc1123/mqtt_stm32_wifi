#include "includes.h"
#include "string.h"
#include "fifo8.h"  


void fifo8_init(struct FIFO8 *fifo,int size, unsigned char *buf)  
/*初始化*/  
{  
	fifo->buf=buf;  
	fifo->flags=0;            
	fifo->free=size;  
	fifo->size=size;  
	fifo->putP=0;                     
	fifo->getP=0;                     
	memset(fifo->buf, '\0', fifo->size);
	return;  
}  
int fifo8_put(struct FIFO8 *fifo,unsigned char data)  
/*向FIFO 中写入数据 */  
{  
	if(fifo->free==0)
	{  
		fifo->flags |= FLAGS_OVERRUN;  
		return -1;  
	}  
	fifo->buf[fifo->putP] = data;  
	fifo->putP++;  
	//循环队列缓冲区  
	if(fifo->putP == fifo->size)
	{  
	    fifo->putP = 0;  
	}  
	fifo->free--;  

	return 0;  
}  

//返回的是数据的位置
int fifo8_get(struct FIFO8 *fifo)  
/*从FIFO 中取出一个数据 */  
{  
	int data;  
	if(fifo->free == fifo->size)
	{  
		return -1;  
	}  
	data = fifo->getP;  
	fifo->getP++;  
	if(fifo->getP == fifo->size)
	{//用来实现循环  
	    fifo->getP = 0;  
	}  
	fifo->free++;  

	return data;  
}  
int fifo8_status(struct FIFO8 *fifo)  
/*缓冲区被使用容量*/  
{  
	return fifo->size-fifo->free;  
}  
int fifo8_free(struct FIFO8 *fifo)  
/*缓冲区剩余容量*/  
{  
	return fifo->free;  
}  

