/******************************************************************************

  Copyright (C), 2001-2011, CSST  Co., Ltd.

 ******************************************************************************
  File Name     : wifi_com.c
  
  Version       : Initial Draft
  
  Author        : kingerzhou
  
  Created       : 2015/11/12
  
  Last Modified :
  
  Description   : this file is to handle all wifi module that is entry to
                  command mode 
  ******************************************************************************/
#include "includes.h"
#include "string.h"
#include "wifi_com.h"
#include "wifi.h"
#include "stdio.h"
#include "uart.h"

#define OFFSET_RETURN_COMMAND   2  //use compare with "at+ok"

  
#define COM_START       "+++"
#define COM_START_A     "a"
#define COM_OK "+ok"
#define COM_AT_E_ON "at+e=on"  //open back display
#define COM_AT_E_OFF "at+e=off"

#define COM_WMODE   "at+wmode"
#define COM_ENTM   "at+entm"
#define COM_WSMAC "at+wsmac"
#define COM_TCPLK "at+tcplk"
#define COM_TCPDIS "at+tcpdis=on"
#define COM_TCPEN "at+tcpdis=off"
#define COM_REBOOT "at+z"

#define RET_TCP_ON "+ok=on"
//#define WIFI_SSID "at+wsssid=Netcore_liu"
//#define WIFI_PWD  "at+wskey=WPA2PSK,TKIP,691e5aaa"
//#define COM_NETP "at+netp=tcp,client,1883,10.0.24.3"
#define WIFI_SSID "at+wsssid=2016"
#define WIFI_PWD  "at+wskey=WPA2PSK,AES,123456789"
#define COM_NETP "at+netp=tcp,client,1883,10.0.24.150"
//#define COM_NETP "at+netp=tcp,client,9001,192.168.1.105"
//#define COM_NETP "at+netp=tcp,client,9182,10.0.24.148"


u8 hexmac[6]={0};
u8 init = 0;


u8 wifi_module_send(u8 *data, u8 len)
{
	uart3_send_data(data, len);
	return len;
}

static s8 wifi_module_chk_ok(CMD_MESSAGE_STRU *msg, char* result, u8 len);
static u8 wifi_module_reboot();


s8 wifi_module_set_tcp_link(u8 enable)
{
	s8 err = -4;
	u8 count=3;
	CMD_MESSAGE_STRU *msg=NULL;
	 while (WIFI_MODE_CMD == wifi_get_mode() && count--)
	 {
		wifi_module_send_command_with_enter_key(enable ? COM_TCPEN : COM_TCPDIS);
		 msg = OSMboxPend(recv_mbox,800,(INT8U *)&err);
		 err = wifi_module_chk_ok(msg, NULL, 0);

		 if(err)
		 {
		 	D_PRINTF("ERR: wifi_module_set_tcp_link\r\n");
		 }
		 else
	 	 {
	 	 	D_PRINTF("wifi_module_set_tcp_link[enable=%d]\r\n", enable);
	 	 }
	 }
	 return err;

}



/*****************************************************************************
* funcation name   : wifi_module_send_command
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
s8  wifi_module_send_command_without_enter_key( char *cmd )
{
    u8 length ;
    s8 ret =0;
    u8 data[48]={0};
    if ( cmd == NULL )
    {
        ret =-1;
        return ret;
    }
    
    length = strlen(cmd);
    memcpy(data,cmd,length);
    uart3_send_data(data,length);
    if(length < 47)
    {
    	*(data + length) = '\0';
    	D_PRINTF("wifi send1:[%s]\r\n", data);
    }
    return ret;   
}

s8  wifi_module_send_command_with_enter_key(char *cmd)
{
    u8 length ;
    s8 ret =0;
    u8 data[48]={0};
    if ( cmd == NULL )
    {
        ret =-1;
        return ret;
    }
    length = strlen(cmd);
    memcpy(data,(u8 *)cmd,length);
    data[length]='\r';
    uart3_send_data(data,length+1);
    if(length < 47)
    {
    	*(data + length) = '\0';
    	D_PRINTF("wifi send2:[%s]\r\n", data);
    }
    return ret;   
}


/*****************************************************************************
* funcation name   : wifi_module_set_networt_protocol_and_port
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
s8  wifi_module_get_network_tcp_connect_status( void)
{
    s8 err;
    u8 count=3;
    CMD_MESSAGE_STRU *msg=NULL;
     while (WIFI_MODE_CMD == wifi_get_mode() && count--)
     {
        wifi_module_send_command_with_enter_key(COM_TCPLK);
         msg = OSMboxPend(recv_mbox,800,(INT8U *)&err);
         if ( msg != NULL )
         {
             if ( msg->length >3)
                {
                    if ( strcmp((const char *)&msg->data[OFFSET_RETURN_COMMAND],RET_TCP_ON) >=0 )
                    {
                       D_PRINTF("tcp is connected\r\n");
                       return 0;
                    }
                }
         }
     }
     return -1;
}


/*****************************************************************************
* funcation name   : wifi_module_set_network_ssid_and_pwd
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
s8  wifi_module_set_network_ssid_and_pwd( void)
{
    s8 err;
    u8 count=3;
    CMD_MESSAGE_STRU *msg=NULL;
     while (WIFI_MODE_CMD == wifi_get_mode() && count--)
     {
         wifi_module_send_command_with_enter_key(WIFI_SSID);
         msg = OSMboxPend(recv_mbox,800,(INT8U *)&err);
		 err = wifi_module_chk_ok(msg, NULL, 0);

		 if(err)
		 {
		 	D_PRINTF("ERR: wifi_module_set_network_ssid\r\n");
		 }
		 else
	 	 {
	 	 	D_PRINTF("set network configuration ssid is ok\r\n");
			break;
	 	 }
     }

     if(0 == count)
     {
     	return -1;
     }

	 OSTimeDlyHMSM(0,0,0,500);
	 count = 3;
	 while (WIFI_MODE_CMD == wifi_get_mode() && count-- )
     {
         wifi_module_send_command_with_enter_key(WIFI_PWD);
         msg = OSMboxPend(recv_mbox,800,(INT8U *)&err);
		 err = wifi_module_chk_ok(msg, NULL, 0);

		 if(err)
		 {
		 	D_PRINTF("ERR: wifi_module_set_network_pwd\r\n");
		 }
		 else
	 	 {
	 	 	D_PRINTF("set network configuration pwd is ok\r\n");
			return 0;
	 	 }
     }
	 
     return -1;
}


/*****************************************************************************
* funcation name   : wifi_module_set_networt_protocol_and_port
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
s8  wifi_module_set_network_protocol_and_port( void)
{
     s8 err;
    u8 count=3;
    CMD_MESSAGE_STRU *msg=NULL;
     while (WIFI_MODE_CMD == wifi_get_mode() && count-- )
     {
        wifi_module_send_command_with_enter_key(COM_NETP);
         msg = OSMboxPend(recv_mbox,800,(INT8U *)&err);
		 err = wifi_module_chk_ok(msg, NULL, 0);

		 if(err)
		 {
		 	D_PRINTF("ERR: wifi_module_set_network_protocol_and_port\r\n");
		 }
		 else
	 	 {
	 	 	D_PRINTF("set network configuration is ok\r\n");
			return 0;
	 	 }
     }
	 
     return -1;
}
/*****************************************************************************
* funcation name   : wifi_module_set_back_display_onoff
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
s8  wifi_module_set_back_display_off( void )
{
    s8 err;
    u8 count=3;
    CMD_MESSAGE_STRU *msg=NULL;
     while (WIFI_MODE_CMD == wifi_get_mode() && count-- )
     {
        wifi_module_send_command_with_enter_key(COM_AT_E_OFF);
         msg = OSMboxPend(recv_mbox,800,(INT8U *)&err);
		 err = wifi_module_chk_ok(msg, NULL, 0);

		 if(err)
		 {
		 	D_PRINTF("ERR: turn off back display\r\n");
		 }
		 else
	 	 {
	 	 	D_PRINTF("turn off back display\r\n");
			return 0;
	 	 }
     }
     return -1;
}

/*****************************************************************************
* funcation name   : wifi_module_set_back_display_onoff
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
s8  wifi_module_get_module_mac( u8 *hexmac )
{
    s8 err;
    char strmac[13];
    u8 count=3;
    CMD_MESSAGE_STRU *msg=NULL;
     while (WIFI_MODE_CMD == wifi_get_mode() && count-- )
     {
        wifi_module_send_command_with_enter_key(COM_WSMAC);
        msg = OSMboxPend(recv_mbox,800,(INT8U *)&err);
		err = wifi_module_chk_ok(msg, strmac, 12);

		if(err)
		{
			D_PRINTF("ERR[%d]: wifi_module_get_module_mac\r\n", err);
		}
		else
		{
			D_PRINTF("wifi module mac:%s", strmac);
            sscanf(strmac, "%2x%2x%2x%2x%2x%2x", hexmac+0,  hexmac+1,  hexmac+2,  hexmac+3, hexmac+4,  hexmac+5); 
            {
                u8 i;
                D_PRINTF("\r\n mac:");
                for ( i = 0 ; i < 6; i++ )
                {
                    D_PRINTF("0x%02x ", hexmac[i]);
                }
                D_PRINTF("\r\n");
            }
			return 0;
		}
     }
	 
     return -1;
}

/*****************************************************************************
* funcation name   : wifi_module_entry_command_mode
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
s8 wifi_module_entry_command_mode(void)
{
    u8 err;
    u8 count=3;
    CMD_MESSAGE_STRU *msg=NULL;
	
    while(WIFI_MODE_THROUGHPUT == wifi_get_mode() && count--)
    {
	    wifi_module_send_command_without_enter_key(COM_START); 
	    msg = OSMboxPend(recv_mbox,800,&err);
	    if ( msg !=NULL )
	    {
	         D_PRINTF("err:%d length:%d data:%c\r\n",err,msg->length,msg->data[0]);
	        if((msg->length ==1)&&(msg->data[0] =='a'))
	        {
	            //OSTimeDlyHMSM(0,0,0,300); 
	            wifi_module_send_command_without_enter_key(COM_START_A);
	            //OSTimeDlyHMSM(0,0,0,20);
	            #if 1
	            msg =NULL; //clear, waiting next 
	            count =3;
	             while ( count-- )
	             {
	                 msg = OSMboxPend(recv_mbox,500,&err);
	                 if ( msg == NULL )
	                 {
	                     wifi_module_send_command_without_enter_key(COM_START_A);
	                 }
	                 else
	                 {
	                     if ( msg->length >3)
	                        {
	                            if ( strcmp((const char *)&msg->data[0],COM_OK) >=0 )
	                            {
	                               D_PRINTF("entry to command mode\r\n");
	                               wifi_module_set_back_display_off();
	                               wifi_set_mode_cmd();
	                               return 0;
	                            }
	                        }
	                 }
	             }
	             #endif
	        }
	    }  
    }
	D_PRINTF("ERR: entry to command mode\r\n");
 return -1;

}

/*****************************************************************************
* funcation name   : wifi_module_entry_through_mode
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
s8  wifi_module_entry_through_mode( void )
{
    u8 err;
    u8 count=3;
    CMD_MESSAGE_STRU *msg=NULL;

     while (WIFI_MODE_CMD == wifi_get_mode() && count-- )
     {
        wifi_module_send_command_with_enter_key(COM_ENTM);
         msg = OSMboxPend(recv_mbox,800,&err);
         if ( msg != NULL )
         {
             if ( msg->length >3)
                {
                 //      D_PRINTF("%s\r\n",msg->data);
                    if ( strcmp((const char *)&msg->data[OFFSET_RETURN_COMMAND],COM_OK) >=0 )
                    {
                       D_PRINTF("entry to through mode\r\n");
                       wifi_set_mode_throughput();
                       return 0;
                    }
                }
         }
     }

	 D_PRINTF("ERR: wifi_module_entry_through_mode\r\n");
     return -1;

}

/*****************************************************************************
* funcation name   : wifi_module_init
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
void  wifi_module_init( void )
{	
	u8 err_cnt;
	s8 err;		
	D_PRINTF("\r\n------wifi_module_init enter\r\n");
reboot_wifi:
	err = -1;
	init = 0;
	err_cnt = 30;
    wifi_module_reset();
	OSTimeDlyHMSM(0,0,1,0);
    wifi_module_entry_command_mode();
	OSTimeDlyHMSM(0,0,0,500);
		
	do
	{		
		err_cnt--;
	    err = wifi_module_get_module_mac(hexmac);
	    if(err)
	    {
	    	continue;
	    }
		OSTimeDlyHMSM(0,0,1,500);
		err = wifi_module_set_network_ssid_and_pwd();
	    if(err)
	    {
	    	continue;
	    }
		OSTimeDlyHMSM(0,0,0,500);
	    err = wifi_module_set_network_protocol_and_port();
	}while(err && err_cnt);
	
    // 查询是否连接成功
    err_cnt = 30;
    while(wifi_module_get_network_tcp_connect_status())
	{		
		OSTimeDlyHMSM(0,0,0,500);
		err_cnt--;
		if(0 == err_cnt)
		{
			wifi_module_reboot();
			wifi_set_mode_throughput();
			goto reboot_wifi;
		}
	}
	
    wifi_module_entry_through_mode();
    init = 1;
	D_PRINTF("\r\n------wifi_module_init exit\r\n");
}

u8 wifi_module_init_get(void)
{
	return init;
}

u8 *wifi_module_get_hex_mac(void)
{
    #if 1
    {
       u8 i;
       D_PRINTF("\r\n mac:");
       for ( i = 0 ; i < 6; i++ )
       {
           D_PRINTF("0x%02x ", hexmac[i]);
       }
       D_PRINTF("\r\n");
    }
    #endif
        
        return hexmac;
}

static u8 wifi_module_reboot()
{
	D_PRINTF("reboot wifi module, waiting...");
	wifi_module_send_command_with_enter_key(COM_REBOOT);	
	OSTimeDlyHMSM(0,0,5,0);
	D_PRINTF("  OK\r\n");
	return 0;
}

/*****************************************************************************
* funcation name   : wifi_module_chk_ok，检查返回是否+ok，并获取=后面的参数
*
* input  : msg:消息体，len:为result的所期望的长度
*
* Output  : resutl:返回+ok=后的结果
* 
* Return Value : -3:msg为空，-2:msg的长度不足，-1:检查结果为失败，0:成功
* 
* descryption:
*
*****************************************************************************/
static s8 wifi_module_chk_ok(CMD_MESSAGE_STRU *msg, char* result, u8 len)
{
	if ( msg != NULL )
	{
	 	if ( msg->length >3)
	    {
	        if ( strcmp((const char *)&msg->data[OFFSET_RETURN_COMMAND],COM_OK) >=0 )
	        {
	            if(NULL != result && len > 0)
            	{
            		memcpy(result,(const char *)&msg->data[OFFSET_RETURN_COMMAND + strlen(COM_OK) + 1], len);
                    result[len]='\0';
            	}
	           	return 0;
	        }
			return -1;
	    }
		return -2;
	}

	return -3;
}
  
