/******************************************************************************

  Copyright (C), All rights reserved by csst company. This is unpulished.


 ******************************************************************************
  File Name     : wifi.c
  
  Version       : Initial Draft
  
  Author        : kingezhou
  
  Created       : 2015/11/4
  
  Last Modified :
  
  Description   : This module is to handle all network event

******************************************************************************/
#include "includes.h"
#include "string.h"
#include "stdlib.h"
#include "MQTTClient.h"
#include "wifi_com.h"
#include "fifo8.h"

#define MAX_SEND_DATA        128
#define MAX_RECEIVE_DATA        128

#define MAX_INDEX_RDATA  1       


u8 wifi_module_connect_is_mode = WIFI_MODE_THROUGHPUT; //default is throughput


OS_EVENT *wifi_rx_mbox; // 
OS_EVENT *wifi_tx_mbox; // 
OS_EVENT *sem_mqttread;


/* using transform datas in command mode */
OS_EVENT *recv_mbox; // 
CMD_MESSAGE_STRU recv_msg={0};
OS_EVENT *mqtt_reconnect_mbox;
u8 mqtt_network_cmd = 0;
u8 mqtt_is_connect = 0;


WIFIRX_MESSAGE_STRU wifirx_msg={0}; //receive message
WIFIRX_MESSAGE_STRU wifitx_msg={0}; //send message

typedef struct RECEIVE_DATA_TOTAL
{
 RECEIVE_DATA rdata[MAX_INDEX_RDATA];
 u8 index;
}RECEIVE_DATA_TOTAL_STRU;

typedef struct SEND_DATA_TOTAL
{
 SEND_DATA sdata[MAX_INDEX_RDATA];
 u8 index;
}SEND_DATA_TOTAL_STRU;


RECEIVE_DATA_TOTAL_STRU rdata_total={0};
SEND_DATA_TOTAL_STRU sdata_total={0};

volatile int toStop = 0;
unsigned char mqtt_recv[MAX_RECEIVE_DATA] = {0};
struct FIFO8 mqtt_recv_fifo8 = {NULL, 0, 0, 0, 0, 0};
Client c;	
u32 pub_num = 0;


struct opts_struct
{
	char* clientid;
	int nodelimiter;
	char* delimiter;
	enum QoS qos;
	char* username;
	char* password;
	char* host;
	int port;
	int showtopics;
} opts =
{
	(char*)"18124615747/box1", 0, (char*)"\n", QOS0, NULL, NULL, (char*)"10.0.24.150", 1883, 0
};


void wifi_set_mode_cmd()
{
	wifi_module_connect_is_mode = WIFI_MODE_CMD;
}

void wifi_set_mode_throughput()
{
	wifi_module_connect_is_mode = WIFI_MODE_THROUGHPUT;
}

u8 wifi_get_mode()
{
	return wifi_module_connect_is_mode;
}


void  wifi_receive_data_parse( u8 *data,u8 length );

/*****************************************************************************
* funcation name   : task_of_wifi_send_module_create
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:  task whic send data to server by wifi module
*
*****************************************************************************/
void  task_of_wifi_send_module_create( void *pdata )
{
        u8 err=0;
        WIFI_SEND_MESSAGE_STRU *wifi_msg_que=NULL;
        WIFI_SEND_MESSAGE_STRU msg_data={0};
//        WIFIRX_MESSAGE_STRU *box_tx_msg=NULL;
        u8 sbody[64];
    
     wifi_tx_mbox = OSMboxCreate((void *)0);
     recv_mbox = OSMboxCreate((void *)0);
     OSTimeDlyHMSM(0,0,2,500);
     wifi_module_init();
    
    while ( 1 )
    {
        wifi_msg_que = OSQPend(network_send_qm,10,&err);
        if(wifi_msg_que !=NULL)
        {
            if(err ==OS_NO_ERR)
            {
                //D_PRINTF("send address:0x%08x\r\n", (wifi_msg_que->address));
                memcpy(&msg_data,wifi_msg_que,sizeof(WIFI_SEND_MESSAGE_STRU));
                memcpy(sbody,(void *)(wifi_msg_que->address),wifi_msg_que->size);
                free((u8 *)wifi_msg_que->address);
                free(wifi_msg_que);
                memset(sdata_total.sdata[sdata_total.index].buffer,0,sizeof(sdata_total.sdata[sdata_total.index].buffer));
                //securety_box_prepare_data_to_send(sdata_total.sdata[sdata_total.index].buffer,msg_data.type,sbody,msg_data.size);
            }
            
        }
     
        OSTimeDly(200);         
    }
}

/*****************************************************************************
* funcation name   : wifi_module_post_send_message_on_queue
*
* input  : type => type information
*               body => body of sending data
*               size => size of body data
* Output  :
* 
* Return Value :  -1 as failed , 0 is ok
* 
* descryption: post message on queue which send data to server by wifi module
*
*****************************************************************************/
s8  wifi_module_post_send_message_on_queue( TYPE_INFO_ENUM type,void *body,u16 size )
{
    WIFI_SEND_MESSAGE_STRU *wifi_send_msg=NULL;
    u8 *sbody =NULL;
    u8 ret;

    wifi_send_msg = (WIFI_SEND_MESSAGE_STRU *)malloc(sizeof(WIFI_SEND_MESSAGE_STRU));
    sbody = (u8 *)malloc(size);
    memcpy(sbody,body,size);
    
    if ( wifi_send_msg ==NULL )
    {
        D_PRINTF("wifi_send_msg heart beat malloc error\r\n");
        return -1;
    }

    wifi_send_msg->type = type;
    wifi_send_msg->address=(u32) sbody;
    wifi_send_msg->size = size;
   // D_PRINTF("address:0x%08x \r\n",wifi_send_msg->address);
    ret =OSQPost(network_send_qm,wifi_send_msg);
    if(ret ==OS_NO_ERR)
    {
        D_PRINTF("network send message on queue success\r\n");
    }
    return 0;
}

/*****************************************************************************
* funcation name   : wifi_post_data_to_command_mode
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption: if wifi module mode is command mode, then post data to different command process
*
*****************************************************************************/
static void  wifi_post_data_to_command_mode( u8 *data, u8 length )
{
	int i = 0;
	memset(recv_msg.data,'\0',MAX_WIFI_COMMAND);
	if(0 == wifi_module_init_get())
	{
		memcpy(recv_msg.data,data,length);
		recv_msg.length = length;
		D_PRINTF("wifi cmd recv: [len=%d][%s]\r\n", length, recv_msg.data);
		OSMboxPost(recv_mbox, &recv_msg);
	}
	else
	{
		//D_PRINTF("wifi through recv: [len=%d]", length);
		for(i = 0; i < length; i++)
		{
			fifo8_put(&mqtt_recv_fifo8, *(data + i));
			//D_PRINTF("0x%02x ", *(data + i));
		}
		//D_PRINTF("\r\n");
	}
}

/*****************************************************************************
* funcation name   : wifi_receive_data_parse
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
void  wifi_receive_data_parse( u8 *data,u8 length )
{
    
    wifi_post_data_to_command_mode(data,length);
   
    if(rdata_total.index >0)
    {
    	rdata_total.index--;
    }
    
}

/*****************************************************************************
* funcation name   : task_of_wifi_receive_module_create
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption: This task will receive data from wifi module by uart3
*                        basic on which DMA uart3 store all datas use big DMA buffer size
*
*****************************************************************************/
void  task_of_wifi_receive_module_create( void *pdata )
{
    u8 err;
    WIFIRX_MESSAGE_STRU *network_rx_msg=NULL;
    
    
    while ( 1 )
    {
        network_rx_msg = OSQPend(network_receive_qm,0,&err);
        if(network_rx_msg !=NULL)
        {
            if ( err == OS_ERR_NONE )
            {
               memset(rdata_total.rdata[rdata_total.index].buffer,0,sizeof(rdata_total.rdata[rdata_total.index].buffer));
               memcpy(rdata_total.rdata[rdata_total.index].buffer,(void *)network_rx_msg->address,network_rx_msg->length);
               rdata_total.rdata[rdata_total.index].length= network_rx_msg->length;               
               wifi_receive_data_parse(rdata_total.rdata[rdata_total.index].buffer,rdata_total.rdata[rdata_total.index].length);               
            }
        }
		
        OSTimeDly(50); 
    }
}


/*****************************************************************************
* funcation name   : wifi_module_post_send_message_on_queue
*
* input  : type => type information
*               body => body of sending data
*               size => size of body data
* Output  :
* 
* Return Value :  -1 as failed , 0 is ok
* 
* descryption: post message on queue which send data to server by wifi module
*
*****************************************************************************/
s8  mqtt_pub_message_on_queue( TYPE_INFO_ENUM type, void *body, u16 size)
{
    WIFI_SEND_MESSAGE_STRU *mqtt_pub_msg=NULL;
    u8 *sbody =NULL;
    u8 ret;

    mqtt_pub_msg = (WIFI_SEND_MESSAGE_STRU *)malloc(sizeof(WIFI_SEND_MESSAGE_STRU));
    sbody = (u8 *)malloc(size);
    memcpy(sbody,body,size);
    
    if ( mqtt_pub_msg ==NULL )
    {
        D_PRINTF("wifi_send_msg heart beat malloc error\r\n");
        return -1;
    }

    mqtt_pub_msg->type = type;
    mqtt_pub_msg->address =(u32) sbody;
    mqtt_pub_msg->size = size;
    D_PRINTF("mqtt pub: [%s]\r\n", (char *)mqtt_pub_msg->address);
    ret = OSQPost(mqtt_pub_qm, mqtt_pub_msg);
    if(ret != OS_NO_ERR)
    {
        D_PRINTF("ERR: mqtt_pub_message_on_queue\r\n");
    }
    return 0;
}


static void messageArrived(MessageData* md)
{
	MQTTMessage* message = md->message;

	if (opts.showtopics)
		D_PRINTF("%.*s\t", md->topicName->lenstring.len, md->topicName->lenstring.data);
	if (opts.nodelimiter)
		D_PRINTF("%.*s\r\n", (int)message->payloadlen, (char*)message->payload);
	else
		D_PRINTF("%.*s%s\r\n", (int)message->payloadlen, (char*)message->payload, opts.delimiter);
	//fflush(stdout);
}


void  task_of_mqtt_sub_module_create( void *pdata )
{
	int rc = 0;
	unsigned int command_timeout_ms = 2000;
	unsigned char buf[128];
	unsigned char readbuf[128];
	char* sub_topic = "18124615747/strongbox/in/+";
	Network n;
	MQTTPacket_connectData data = MQTTPacket_connectData_initializer;
	INT8U err = 0;		
	unsigned int cnt_reconnect = 0;
	unsigned int cnt_subscribe = 0;
	mqtt_reconnect_mbox = OSMboxCreate((void *)0);

	D_PRINTF("task_of_mqtt_sub_module_create\r\n");
	
	fifo8_init(&mqtt_recv_fifo8 , MAX_RECEIVE_DATA, mqtt_recv);

reconnect:	
	cnt_subscribe = 0;
	mqtt_is_connect = 0;
	
	while(0 == wifi_module_init_get())
	{
		OSTimeDlyHMSM(0,0,0,1000);
	}
	
	if (strchr(sub_topic, '#') || strchr(sub_topic, '+'))
	{
		opts.showtopics = 1;
	}
	if (opts.showtopics)
	{
		D_PRINTF("sub topic is %s\r\n", sub_topic);
	}

	data.willFlag = 0;
	data.MQTTVersion = 3;
	data.clientID.cstring = opts.clientid;
	data.username.cstring = opts.username;
	data.password.cstring = opts.password;
	data.keepAliveInterval = 10;
	data.cleansession = 1;  //0: Client断开连接后，Server应该保存Client的订阅信息
	opts.qos = QOS0; // 如果需要接收离线消息，qos需大于0，clean为disable
	//订阅者如果希望可以收到离线消息
	//qos需大于0，clean为disable

	D_PRINTF("--------wifi is connecting to mqtt server\r\n");

	NewNetwork(&n);
	ConnectNetwork(&n, opts.host, opts.port);
	MQTTClient(&c, &n, command_timeout_ms, buf, 100, readbuf, 100);   
	
	D_PRINTF("Connecting to %s %d, command_timeout_ms=%d\r\n", opts.host, opts.port, command_timeout_ms);
	//D_PRINTF("data size = %d\r\n", sizeof(MQTTPacket_connectData));
	err = 5 * 60 / 5;
	do
	{			
		rc = MQTTConnect(&c, &data);
		D_PRINTF("Connected %d, %d\r\n", rc, err);
		OSTimeDlyHMSM(0,0,5,0);
		err--;
		if(0 == err && SUCCESS_1 != rc)
		{
			err = 5 * 60 / 5;
			wifi_module_init();
		}
	}while(SUCCESS_1 != rc);

	mqtt_is_connect = 1;

/*
	//处理离线消息
	c.messageHandlers[0].topicFilter = sub_topic;
	c.messageHandlers[0].fp = messageArrived;
	rc = 5; // 5s
	do
	{
		OSMutexPend(sem_mqttread,0,&err); //
		MQTTYield(&c, 200); //这个与waitfor函数对mqttread冲突	
		OSMutexPost(sem_mqttread);
		OSTimeDlyHMSM(0,0,1,0);
	}while(rc--);
	c.messageHandlers[0].topicFilter = 0;
*/
    D_PRINTF("Subscribing to %s\r\n", sub_topic);
	cnt_subscribe = (data.keepAliveInterval + 10) / 2;
    do
    {
		rc = MQTTSubscribe(&c, sub_topic, opts.qos, messageArrived);
		D_PRINTF("Subscribed %d\r\n", rc);
		cnt_subscribe--;
		OSTimeDlyHMSM(0,0,2,0);
	}while(FAILURE == rc && cnt_subscribe);	
	if(SUCCESS_1 != rc && 0 == cnt_subscribe)
	{
		goto reconnect;
	}

	D_PRINTF(">>>>>MQTT sub OK!!!!\r\n");	

	while (!toStop)
	{
		//msg = OSMboxPend(recv_mbox,800,&err);
		//D_PRINTF("sub msg=%s\r\n", msg->data);
		OSMboxPend(mqtt_reconnect_mbox, 50, &err);
		if(OS_ERR_NONE == err && 1 == mqtt_network_cmd)
		{			
            mqtt_network_cmd = 0;
			cnt_reconnect++;
			D_PRINTF("mqtt reconnect cnt[%d]\r\n", cnt_reconnect);
			goto reconnect;
		}
		OSMutexPend(sem_mqttread,0,&err); //
		MQTTYield(&c, 500); //这个与waitfor函数对mqttread冲突	
		OSMutexPost(sem_mqttread);
		OSTimeDlyHMSM(0,0,0,200);
	}
	
	D_PRINTF(">>>>>MQTT Stopping\r\n");

	MQTTDisconnect(&c);
	n.disconnect(&n);	
}

void  task_of_mqtt_pub_module_create( void *pdata )
{
	u8 err;
	u8 mqtt_pub_key;
	u8 flag = 1;
	WIFI_SEND_MESSAGE_STRU *mqtt_pub_msg=NULL;	
	char* pub_topic_alarm = "18124615747/strongbox/out/alarm";
	char* pub_topic_temp =  "18124615747/strongbox/out/temp";
	MQTTMessage message;
	char msg[32] = {0};

	D_PRINTF("task_of_mqtt_pub_module_create\r\n");
		
	while ( 1 )
	{	
		mqtt_pub_key= mqtt_pub_key_scan();

		if(mqtt_is_connect && flag && mqtt_pub_key > 0)
		{			
			memset(msg, '\0', 32);
			sprintf(msg, "%s%04d", "pub_wcliu", pub_num++);
			flag = 0;

			if(2 == mqtt_pub_key)
			{				 
				mqtt_pub_message_on_queue(ALARM_INFO, msg, 32);
			}
			else if(3 == mqtt_pub_key)
			{				 
				mqtt_pub_message_on_queue(TEMPERATURE, msg, 32);
			}
		}		
		
		mqtt_pub_msg = OSQPend(mqtt_pub_qm,50,&err);
		if(mqtt_pub_msg != NULL)
		{
			if ( err == OS_ERR_NONE )
			{
				// 发布时，如果需要让订阅者可以收到离线的消息
				//qos需大于0，retain为1
			   message.qos = QOS1; //>QoS0: 配合retained，才能把离线的消息重新发送
			   message.retained = 1; // 1:要求服务器需要持久保存此消息
			   message.dup = 0;
			   message.payload = (void *)mqtt_pub_msg->address;
			   message.payloadlen = mqtt_pub_msg->size;
			   OSMutexPend(sem_mqttread,0,&err); //
			   if(ALARM_INFO == mqtt_pub_msg->type)
			   {
				   if(MQTTPublish(&c, pub_topic_alarm, &message))
				   {
						D_PRINTF("ERR: pub alarm message\r\n");
				   }
			   }
			   else if(TEMPERATURE == mqtt_pub_msg->type)
			   {
				   if(MQTTPublish(&c, pub_topic_temp, &message))
				   {
						D_PRINTF("ERR: pub temperature message\r\n");
				   }
			   }
			   else
			   {
			       D_PRINTF("ERR: msg type is unknown\r\n");
			   }
			   OSMutexPost(sem_mqttread);

			   OSTimeDlyHMSM(0,0,1,0);

			   flag = 1;
			}
			
		   free((u8 *)mqtt_pub_msg->address);
           free(mqtt_pub_msg);
		}
		
		OSTimeDly(50); 
	}
}



