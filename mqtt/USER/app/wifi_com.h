#ifndef __WIFI_COM_H
#define __WIFI_COM_H

s8 wifi_module_entry_command_mode(void);
s8 wifi_module_entry_through_mode(void);
s8  wifi_module_set_network_protocol_and_port( void);
s8  wifi_module_get_network_tcp_connect_status( void);

s8  wifi_module_send_command_without_enter_key( char *cmd );
s8  wifi_module_send_command_with_enter_key(char *cmd);

u8 *wifi_module_get_hex_mac(void);
void wifi_module_init(void);
u8 wifi_module_init_get(void);
u8 wifi_module_send(u8 *data, u8 len);
s8 wifi_module_set_tcp_link(u8 enable);





#endif /* __WIFI_COM_H */
