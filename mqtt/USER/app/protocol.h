#ifndef __PROTOCOL_H
#define __PROTOCOL_H

//#pragma pack(1) //��Ӱ��mqtt����

typedef enum TYPE_INFO
{
    ALARM_INFO =1,
    UNLOCK_INFO,
    SETTING_CMD,
    TEMPERATURE,
}TYPE_INFO_ENUM;

typedef struct WIFI_SEND_MESSAGE
{
    TYPE_INFO_ENUM type;
    u32 address;
    u16 size;
}WIFI_SEND_MESSAGE_STRU;

#endif /* __PROTOCOL_H */
