/******************************************************************************

  Copyright (C), 2001-2011, CSST Co., Ltd.

 ******************************************************************************
  File Name     : common.c
  
  Version       : Initial Draft
  
  Author        : kingerzhou
  
  Created       : 2015/11/19
  
  Last Modified :
  
  Description   : this file is to supply some common function for others
                  application

******************************************************************************/
/*****************************************************************************
* funcation name   : common_xor_checksum_calculate
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
#include"includes.h"
#include "string.h"
#include "stdlib.h"
#include "common.h"


#define ALARM_INFO_START_ADDRESS    (0x08000000+0x1e000)    // 121k flash page
#define UNLOCK_INFO_START_ADDRESS    (0x08000000+0x1e400)    // 122k flash page
#define EKEY_DATA_BASE_START_ADDRESS    (0x08000000+0x1e800)    // 123k flash page
#define NUMBER_OFFSET       2

//#pragma pack(1) //��Ӱ��mqtt����
typedef struct EKEY_DATA_BASE
{
    EKEY_OBJECT_STRU obj;
    u16 num;  // flash write must be mutiple of 2
}EKEY_DATA_BASE_STRU;

typedef struct ALARM_INFO_BASE
{
    ALARM_INFO_STRU alarm;
    u8 num;
}ALARM_INFO_BASE_STRU;

typedef struct UNLOCK_INFO_BASE
{
    UNLOCK_INFO_STRU unlock;
    u16 num;
}UNLOCK_INFO_BASE_STRU;
/*****************************************************************************
* funcation name   : common_xor_checksum_calculate
*
* input  :  *data =>  which store data
*                length => how many datas
* Output  :
* 
* Return Value :  the checksum of xor
* 
* descryption:  calculate the xor of some datas
*
*****************************************************************************/
u8  common_xor_checksum_calculate( u8 *data,u8 length)
{
    u8 xor=0;
    u8 i;

    if ( data == NULL )
    {
        return 0;
    }
    for ( i = 0 ; i <length ; i++ )
    {
        xor ^=data[i];
    }
    return xor;
}

void flash_write_bytes(u32 addr , u8 *data , u16 length)  
{  
        u32 HalfWord;  
        length = length/2;  
        FLASH_Unlock();  
        FLASH_ClearFlag(FLASH_FLAG_BSY | FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);  
        FLASH_ErasePage(addr);  
        while(length --)  
        {  
        HalfWord=*(data++);  
        HalfWord|=*(data++)<<8;  
        FLASH_ProgramHalfWord(addr, HalfWord);  
        addr += 2;  
        }  
        FLASH_Lock();  
}


void flash_read_bytes(u32 addr , u8 *data , u16 length)  
{  
      while(length--)  
      {  
       *(data++)=*((u8*)addr++);  
      }  
}  

void test_array(void )
{
   u8 wdata[]={1,2,3,4,5,6,7,8,9,0};
    u8 data[]={21,22,22,24,25};
   u8 rdata[10]={0};

   flash_write_bytes(ALARM_INFO_START_ADDRESS, wdata, sizeof(wdata));
  flash_write_bytes(ALARM_INFO_START_ADDRESS, data, sizeof(data));
   D_PRINTF("size :%d\r\n", sizeof(wdata));
   flash_read_bytes(ALARM_INFO_START_ADDRESS,rdata,9);
   {
        u8 i;
        D_PRINTF("\r\nrdata:");
        for ( i = 0 ; i <10 ; i++ )
        {
            D_PRINTF(" %d ", rdata[i]);
        }
        D_PRINTF("\r\n");
   }

}

/*****************************************************************************
* funcation name   : get_flash_obj_data_base_number
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
u16  get_flash_obj_data_base_number( u32 address )
{
    u16 ret=0;
    u8 data[2];
    
    flash_read_bytes(address,data,sizeof(data));
    ret = (data[0]<<8)|data[1];
    return ret;
}

/*****************************************************************************
* funcation name   : set_flash_obj_data_base_number
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
void  set_flash_obj_data_base_number( u32 address ,u16 num)
{
    u8 data[2];

    data[0] = (num>>8)&0xff;
    data[1] = (u8)(num&0xff);

    flash_write_bytes(address,data,sizeof(data));
    
    
}

/*****************************************************************************
* funcation name   : add_object_to_flash_data_base
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
s8  add_object_to_flash_data_base( u32 address, void *obj, u8 obj_size )
{
    u16 num=0;
    u8 *data=NULL;
    u16 count=0;
    
    if (obj == NULL )
    {
        return -1;
    }

     num = get_flash_obj_data_base_number(address);

    if ( num == 0xffff )    // flash after erasing ,the default value is 0xff
    {
        num =0;
    }
    
    if ( num == 0 )
    {
        data = (u8 *)malloc(obj_size);
        data[count++] =((num+1)>>8)&0xff;
        data[count++] =(u8) ((num+1)&0xff);
        memcpy(data+count,(u8 *)obj,obj_size);
        count += obj_size;
    }
    else
    {
        
        data = (u8 *)malloc((num+1)*obj_size);
        data[count++] =((num+1)>>8)&0xff;
        data[count++] =(u8) ((num+1)&0xff);
        flash_read_bytes(address+NUMBER_OFFSET, data+count,num*obj_size);
        memcpy(data+count+num*obj_size,(u8 *)obj,obj_size);
        count +=(num+1)*obj_size;
    }

    flash_write_bytes(address,data,count);
    
    free(data);

    return 0;
}

/*****************************************************************************
* funcation name   : just_ekey_obj_exist_data_base
*
* input  : data => data base
*               num => how many objects store in data base, maximum =20
*               obj => ekey object
*
* Output  : if found it in data base, then give object ekey and epwd
* 
* Return Value : -1 is failed, 0 is ok ,which find it 
* 
* descryption: find ekey obj is exist in data base
*
*****************************************************************************/
s8 just_ekey_obj_exist_data_base( u8 *data,u8 num,EKEY_OBJECT_STRU *obj)
{
    s8 ret;
    u8 i=0;
    
    if ( data == NULL ||obj == NULL )
    {
        return -1;
    }
    
    for ( i = 0 ; i <num ; i++ )
    {
       ret = memcmp(data+(i*sizeof(EKEY_OBJECT_STRU)),obj->devID,8);
       if ( ret == 0 )
       {
           memcpy(obj->ekey,data+(i*sizeof(EKEY_OBJECT_STRU))+sizeof(obj->devID),sizeof(obj->ekey));
           memcpy(obj->epwd,data+(i*sizeof(EKEY_OBJECT_STRU))+sizeof(obj->devID)+sizeof(obj->ekey),sizeof(obj->epwd));
           return 0;
       }
       
    }
    
    return -1;

        
}

/*****************************************************************************
* funcation name   : ekey_object_judge_devID_and_password_in_data_base
*
* input  : data => data base
*               num => how many objects store in data base, maximum =20
*               obj => ekey object
*
* Output  :
* 
* Return Value : -1 is failed, 0 is ok ,which ekey object is correct 
* 
* descryption: check ekey objcet devID and password
*
*****************************************************************************/
s8 ekey_object_judge_devID_and_password_in_data_base( u8 *data,u8 num,EKEY_OBJECT_STRU *obj)
{
    s8 ret;
    u8 i=0;
    
    if ( data == NULL ||obj == NULL )
    {
        return -1;
    }
    
    for ( i = 0 ; i <num ; i++ )
    {
       ret = memcmp(data+(i*sizeof(EKEY_OBJECT_STRU)),obj->devID,sizeof(obj->devID));
       if ( ret == 0 )
       {
           D_PRINTF("ekey data base find object devID\r\n");
           if(memcmp(data+(i*sizeof(EKEY_OBJECT_STRU))+sizeof(obj->devID),obj->epwd,sizeof(obj->epwd)) == 0)
           {
               return 0;
           }

       }
       
    }
    
    return -1;

        
}

/*****************************************************************************
* funcation name   : securety_box_add_ekey_obj_to_data_base
*
* input  :  address => ekey object data base start address which is often start of one page on flash
*               obj => pointer to ekey object struction
*               obj_size => size of ekey object struction
*
* Output  :
* 
* Return Value : -1 is failed, 0 is ok, 1 is which the obj had been exist in data base
* 
* descryption: add ekey object to data base , and write it to flash
*
*****************************************************************************/

s8  securety_box_add_ekey_obj_to_data_base(u32 address, EKEY_OBJECT_STRU *obj,u8 obj_size)
{
    u16 num=0;
    u8 *data=NULL;
    u16 count=0;
    s8 ret =-1;

    if( obj == NULL )
    {
        return -1;
    }

     num = get_flash_obj_data_base_number(address);

    if ( num == 0xffff )    // flash after erasing ,the default value is 0xff
    {
        num =0;
    }

    if ( num == 0 )
    {
        data = (u8 *)malloc(obj_size);
        data[count++] =((num+1)>>8)&0xff;
        data[count++] =(u8) ((num+1)&0xff);
        memcpy(data+count,(u8 *)obj,obj_size);
        count += obj_size;
    }
    else
    {
        
        data = (u8 *)malloc((num+1)*obj_size);
        data[count++] =((num+1)>>8)&0xff;
        data[count++] =(u8) ((num+1)&0xff);
        flash_read_bytes(address+NUMBER_OFFSET, data+count,num*obj_size);
        ret = just_ekey_obj_exist_data_base(data+count,num,obj);
        if ( ret ==0 )
        {
            return 1;   // find it have been exist in data base
        }
        memcpy(data+count+num*obj_size,(u8 *)obj,obj_size);
        count +=(num+1)*obj_size;
    }

    flash_write_bytes(address,data,count);

    free(data);

    return 0;
       
    
    
}


s8 AES_128bits_un_encrytion(u8 *in_data,u8 *out_data,u8 length)
{
   return 0;
}

/*****************************************************************************
* funcation name   : commom_check_ekey_object_in_data_base
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
s8  commom_check_ekey_object_in_data_base( EKEY_OBJECT_STRU *ekey_obj )
{
    u16 num=0;
    u8 *data=NULL;
    s8 ret =-1;

    if( ekey_obj== NULL )
    {
        return -1;
    }

     num = get_flash_obj_data_base_number(EKEY_DATA_BASE_START_ADDRESS);
     if ( num == 0xffff )    // flash after erasing ,the default value is 0xff
    {
        num =0;
    }

     if ( num == 0 )
     {
          D_PRINTF("no ekey object in the data base\r\n");
          return -1;
     }
     else
     {
        data = (u8 *)malloc(num*sizeof(EKEY_OBJECT_STRU));
        flash_read_bytes(EKEY_DATA_BASE_START_ADDRESS+NUMBER_OFFSET, data,num*sizeof(EKEY_OBJECT_STRU));
        ret = ekey_object_judge_devID_and_password_in_data_base(data,num,ekey_obj);
        if ( ret == 0 )
        {
            free(data);
            return 0;
        }
        else
        {
            free(data);
            return -1;
        }
        
     }

}
