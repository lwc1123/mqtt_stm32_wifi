#ifndef __MSG_QUEUE_H
#define __MSG_QUEUE_H

typedef struct UNLOCK_INFORMATION
{
    u8 devID[8];
    u8 password[8];
}UNLOCK_INFORMATION_STRU;


extern OS_EVENT * unlock_qm;

extern OS_EVENT * network_send_qm;

extern OS_EVENT * network_receive_qm;

extern OS_EVENT * mqtt_sub_qm;

extern OS_EVENT * mqtt_pub_qm;




void  create_network_send_message_queue_between_tasks( void );
void  create_network_receive_message_queue_between_tasks( void );
void create_mqtt_sub_message_queue_between_tasks(void);
void create_mqtt_pub_message_queue_between_tasks(void);


#endif /* __MSG_QUEUE_H */

