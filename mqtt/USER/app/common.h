#ifndef __COMMON_H
#define __COMMON_H



//#pragma pack(1) //��Ӱ��mqtt����
typedef struct EKEY_OBJECT
{
    u8 devID[8];
    u8 epwd[8];
    u8 random[8];
    u8 ekey[16];
}EKEY_OBJECT_STRU;

typedef struct ALARM_INFO
{
    u8 obj;
    u8 rtime[8];    //real time
    u8 data[2];             
}ALARM_INFO_STRU;

typedef struct UNLOCK_INFO
{
    u8 obj[8];
    u8 rtime[8];    //real time             
}UNLOCK_INFO_STRU;



u8  common_xor_checksum_calculate( u8 *data,u8 length);
void test_array(void );
s8 AES_128bits_un_encrytion(u8 *in_data,u8 *out_data,u8 length);
s8  commom_check_ekey_object_in_data_base( EKEY_OBJECT_STRU *ekey_obj );


#endif /* __COMMON_H */
