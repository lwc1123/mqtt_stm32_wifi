/******************************************************************************

  Copyright (C), All rights resolved by CSST company, This is unpulished.


 ******************************************************************************
  File Name     : msg_queue.c
  
  Version       : Initial Draft
  
  Author        : kingerzhou
  
  Created       : 2015/11/5
  
  Last Modified :
  
  Description   : this module is to handle all message queue
******************************************************************************/
#include "includes.h"  
/*----------------------------------------------*
 * macros                                       *
 *----------------------------------------------*/
 
 /* one message queue can store how many messages */
#define MAX_NUMBER_IN_MESSAGE_GROUP             16

OS_EVENT * network_receive_qm=NULL; //receive data from wifi module via uart
OS_EVENT * network_send_qm=NULL; //send data from wifi module via uart
OS_EVENT * mqtt_sub_qm=NULL;
OS_EVENT * mqtt_pub_qm=NULL;


/*----------------------------------------------*
 * project-wide global variables                *
 *----------------------------------------------*/
 /* send and receive the unlock message */



/* message group which is store messages */  
void *network_receive_msg_group[MAX_NUMBER_IN_MESSAGE_GROUP];
void *network_send_msg_group[MAX_NUMBER_IN_MESSAGE_GROUP];
void *mqtt_sub_msg_group[MAX_NUMBER_IN_MESSAGE_GROUP];
void *mqtt_pub_msg_group[MAX_NUMBER_IN_MESSAGE_GROUP];



/*----------------------------------------------*
 * module-wide global variables                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * constants                                    *
 *----------------------------------------------*/


/*----------------------------------------------*
 * routines' implementations                    *
 *----------------------------------------------*/



/*****************************************************************************
* funcation name   : create_network_send_message_queue_between_tasks
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
void  create_network_send_message_queue_between_tasks( void )
{
     network_send_qm=OSQCreate(&network_send_msg_group[0],MAX_NUMBER_IN_MESSAGE_GROUP);
    if(network_send_qm !=NULL)
    {
        D_PRINTF("create network send message queue success\r\n");
    }
}

/*****************************************************************************
* funcation name   : create_network_receive_message_queue_between_tasks
*
* input  : 
*
* Output  :
* 
* Return Value : 
* 
* descryption:
*
*****************************************************************************/
void  create_network_receive_message_queue_between_tasks( void )
{
     network_receive_qm=OSQCreate(&network_receive_msg_group[0],MAX_NUMBER_IN_MESSAGE_GROUP);
     if(network_receive_qm !=NULL)
    {
        D_PRINTF("create network receive message queue success\r\n");
    }
}


void  create_mqtt_sub_message_queue_between_tasks( void )
{
     mqtt_sub_qm = OSQCreate(&mqtt_sub_msg_group[0],MAX_NUMBER_IN_MESSAGE_GROUP);
     if(mqtt_sub_qm !=NULL)
    {
        D_PRINTF("create_mqtt_sub_message_queue_between_tasks success\r\n");
    }
}

void  create_mqtt_pub_message_queue_between_tasks( void )
{
     mqtt_pub_qm=OSQCreate(&mqtt_pub_msg_group[0],MAX_NUMBER_IN_MESSAGE_GROUP);
     if(mqtt_pub_qm !=NULL)
    {
        D_PRINTF("create_mqtt_pub_message_queue_between_tasks success\r\n");
    }
}



