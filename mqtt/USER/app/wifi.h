#ifndef __WIFI_H
#define __WIFI_H

#include "protocol.h"
/* max datas in wifi module command mode */
#define MAX_WIFI_COMMAND    128
#define WIFI_MODE_CMD          0
#define WIFI_MODE_THROUGHPUT   1


typedef struct WIFIRX_MESSAGE
{
    u32 address;
    u8 length;  
    u8 type;
}WIFIRX_MESSAGE_STRU;

typedef struct CMD_MESSAGE
{
    u8 data[MAX_WIFI_COMMAND];
    u8 length;
}CMD_MESSAGE_STRU;


void  task_of_wifi_send_module_create( void *pdata);
void  task_of_wifi_receive_module_create( void *pdata);
void  task_of_mqtt_sub_module_create( void *pdata);
void  task_of_mqtt_pub_module_create( void *pdata);

s8  wifi_module_post_send_message_on_queue( TYPE_INFO_ENUM type,void *body,u16 size );
void wifi_set_mode_cmd(void);
void wifi_set_mode_throughput(void);
u8 wifi_get_mode(void);



extern OS_EVENT *wifi_rx_mbox; // 
extern OS_EVENT *wifi_tx_mbox; // 
extern WIFIRX_MESSAGE_STRU wifirx_msg; //receive message
extern WIFIRX_MESSAGE_STRU wifitx_msg; //send message

extern OS_EVENT *recv_mbox; // 
extern CMD_MESSAGE_STRU recv_msg;


#endif /* __WIFI_H */
